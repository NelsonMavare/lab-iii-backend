const express = require('express');
const CountryController = require('../controllers/CountryController');
const auth = require('../middlewares/authentication');

const router = express.Router();

router.post('', auth.authenticate, CountryController.create);
router.get('/:id', auth.authenticate, CountryController.show);
router.get('', CountryController.index);
router.put('/:id', auth.authenticate, CountryController.update);
router.delete('/:id', auth.authenticate, CountryController.delete);

module.exports = router;