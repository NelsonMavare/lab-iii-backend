const express = require("express");
const ApplicationController = require("../controllers/ApplicationController");

const router = express.Router();

router.post('/login', ApplicationController.login.bind(ApplicationController));
router.post('/register', ApplicationController.register.bind(ApplicationController));
router.post('/forgot_password', ApplicationController.forgot_password.bind(ApplicationController));
router.post('/average', ApplicationController.average);

module.exports = router;
