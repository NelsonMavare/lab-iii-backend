const express = require('express');
const UserController = require('../controllers/UserController')
const auth = require('../middlewares/authentication');

const router = express.Router();

router.post('/change_password', auth.authenticate, UserController.changePassword);
router.post('',  UserController.create);
router.put('/:id', auth.authenticate, UserController.update);
router.get('/:id', [auth.authenticate, UserController.exists], UserController.show);
router.get('', auth.authenticate, UserController.index);
router.delete('/:id', [auth.authenticate, UserController.exists], UserController.delete);
router.post('/:id/reactivate', auth.authenticate, UserController.reactivate);
router.post('/:id/uploadProfileImage', auth.authenticate, UserController.uploadProfileImage);
router.post('/monthly_gains', auth.authenticate, UserController.total_gain);
router.post('/averageUserScore', auth.authenticate, UserController.averageUserScore);
router.post('/popular_users', auth.authenticate, UserController.popular_users);


module.exports = router;
