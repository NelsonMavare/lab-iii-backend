const express = require('express');
const RoleController = require('../controllers/RoleController')
const auth = require('../middlewares/authentication');

const router = express.Router();

router.get('',  RoleController.index);
router.get('/:id', auth.authenticate, RoleController.show);
router.post('', auth.authenticate, RoleController.create);
router.put('/:id', auth.authenticate, RoleController.update);
router.delete('/:id', auth.authenticate, RoleController.delete);
router.post('/:id/reactivate', auth.authenticate, RoleController.reactivate);

module.exports = router;
