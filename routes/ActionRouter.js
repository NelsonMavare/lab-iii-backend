const express = require('express');
const ActionController = require('../controllers/ActionController')
const auth = require('../middlewares/authentication');

const router = express.Router();

router.get('', auth.authenticate, ActionController.index);
router.get('/:id', auth.authenticate, ActionController.show);
router.post('', auth.authenticate, ActionController.create);
router.put('/:id', auth.authenticate, ActionController.update);
router.delete('/:id', auth.authenticate, ActionController.delete);
router.post('/:id/reactivate', auth.authenticate, ActionController.reactivate);

module.exports = router;
