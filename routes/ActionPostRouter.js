const express = require('express');
const ActionPostController = require('../controllers/ActionPostController')
const auth = require('../middlewares/authentication');

const router = express.Router();

router.post('/like', auth.authenticate, ActionPostController.like);
router.post('/liked_posts', auth.authenticate, ActionPostController.liked_posts);
router.put('/remove_like', auth.authenticate, ActionPostController.remove_like);

module.exports = router;
