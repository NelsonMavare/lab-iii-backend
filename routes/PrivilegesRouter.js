const express = require('express');
const PrivilegeController = require('../controllers/PrivilegeController');
const auth = require('../middlewares/authentication');

const router = express.Router();

router.post('', auth.authenticate, PrivilegeController.create);
router.get('', auth.authenticate, PrivilegeController.index);
router.get('/:id', auth.authenticate, PrivilegeController.show);
router.put('/:id', auth.authenticate, PrivilegeController.update);
router.delete('/:id', auth.authenticate, PrivilegeController.delete);

module.exports = router;