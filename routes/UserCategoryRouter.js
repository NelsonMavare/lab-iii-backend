const express = require('express');
const UserCategoryController = require('../controllers/UserCategoryController');
const auth = require('../middlewares/authentication');

const router = express.Router();

router.post('/follow', auth.authenticate, UserCategoryController.follow);
router.put('/unfollow', auth.authenticate, UserCategoryController.unfollow);
router.post('/timeline', auth.authenticate, UserCategoryController.timeline);
router.get('/total_followers', auth.authenticate, UserCategoryController.total_followers);
router.post('/followed_categories', auth.authenticate, UserCategoryController.followed_categories);
router.get('/popular_categories', auth.authenticate, UserCategoryController.popular_categories);

module.exports = router;
