const express = require('express');
const ShareController = require('../controllers/ShareController');
const auth = require('../middlewares/authentication');

const router = express.Router();

router.post('/send_mail', auth.authenticate, ShareController.share_post);

module.exports = router;