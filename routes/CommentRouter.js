const express = require('express');
const CommentController = require('../controllers/CommentController')
const auth = require('../middlewares/authentication');

const router = express.Router();

router.post('/publish', auth.authenticate, CommentController.publish_comment);
router.post('', auth.authenticate, CommentController.seller_comments);

module.exports = router;
