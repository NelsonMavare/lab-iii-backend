const express = require('express');
const PostController = require('../controllers/PostController')
const UserController = require('../controllers/UserController')
const CategoryController = require('../controllers/CategoryController');
const auth = require('../middlewares/authentication');

const router = express.Router();

router.get('', auth.authenticate, PostController.index);
router.post('', [auth.authenticate, CategoryController.exists], PostController.post);
router.post('/filter', auth.authenticate, PostController.filter);
router.post('/user', [auth.authenticate, UserController.existsById], PostController.getPostsByUser);
router.post('/likes_count', auth.authenticate, PostController.likes_count);
router.put('/delete', auth.authenticate, PostController.delete);

module.exports = router;
