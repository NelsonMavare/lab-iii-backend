const express = require('express');
const PurchaseController = require('../controllers/PurchaseController');
const auth = require('../middlewares/authentication');

const router = express.Router(); 

router.post('', auth.authenticate, PurchaseController.create); 
router.post('/seller_purchases', auth.authenticate, PurchaseController.seller_purchases); //trae las solicitudes de compra de un VEDEDOR
router.post('/buyer_purchases', auth.authenticate, PurchaseController.buyer_purchases); //trae las solicitudes de compra de un COMPRADOR
router.put('/accept', auth.authenticate, PurchaseController.accept_request);
router.put('/cancel', auth.authenticate, PurchaseController.cancel_request);
router.put('/reject', auth.authenticate, PurchaseController.reject_request);
router.put('/finish', auth.authenticate, PurchaseController.finish_purchase_request);
router.get('/download_image/:id',PurchaseController.downloadFileImage)


module.exports = router;