const ApplicationRouter = require('./ApplicationRouter');
const UserRouter = require('./UsersRouter');
const RoleRouter = require('./RoleRouter');
const ActionRouter = require('./ActionRouter');
const CategoryRouter = require('./CategoryRouter');
const CountryRouter = require('./CountriesRouter');
const PrivilegeRouter = require('./PrivilegesRouter');
const ShareRouter = require('./ShareRouter');
const PostRouter = require('./PostRouter');
const ActionPostRouter = require('./ActionPostRouter');
const UserCategoryRouter = require('./UserCategoryRouter');
const PurchaseRouter = require('./PurchaseRouter');
const CommentRouter = require('./CommentRouter');

exports.load = app => {
 
	app.use('', ApplicationRouter);
	app.use('/users', UserRouter);
	app.use('/roles', RoleRouter);
	app.use('/actions', ActionRouter);
	app.use('/categories', CategoryRouter);
	app.use('/countries', CountryRouter);
	app.use('/privileges', PrivilegeRouter);
	app.use('/share', ShareRouter);
	app.use('/posts', PostRouter);
	app.use('/action_post', ActionPostRouter);
	app.use('/user_category', UserCategoryRouter);
	app.use('/purchase', PurchaseRouter);
	app.use('/comment', CommentRouter);


  return app;
};
