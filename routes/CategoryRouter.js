const express = require('express');
const CategoryController = require('../controllers/CategoryController')
const auth = require('../middlewares/authentication');

const router = express.Router();

router.get('',  CategoryController.index);
router.get('/:id', auth.authenticate, CategoryController.show);
router.post('', auth.authenticate, CategoryController.create);
router.put('/:id', auth.authenticate, CategoryController.update);
router.delete('/:id', auth.authenticate, CategoryController.delete);
router.post('/:id/reactivate', auth.authenticate, CategoryController.reactivate);

module.exports = router;
