"use strict";
const Country = require('../models/Country');
const CountryTransformer = require("../transformers/CountryTransformer");
//const {check} = require('express-validator/check');

class CountryController{
	constructor(){
		this.create = this.create.bind(this);
		this.update = this.update.bind(this);
		this.show = this.show.bind(this);
		this.index = this.index.bind(this);
		this.delete = this.delete.bind(this);

	}

	async create(req, res){
		const name = req.body.name;

		req.checkBody('name', 'El campo nombre no puede estar vacio').notEmpty();
		let errors = req.validationErrors();

		if(errors){
			return res.status(500).json({
				status: 500,
				errors
			});
		}

		else{
			var current_date = Date.now();
			let country = new Country({
				name: name,
				created_at: current_date,
				updated_at: current_date
			});

			country.save(function(error, countryDB){
				if(error){
					return res.status(500).json({
						status: 500,
						error
					})
				}
				else{
					console.log(country);
					return res.json({
						status: 200, 
						message: 'Pais registrado exitosamente',
						country: CountryTransformer.transform(countryDB)
					});	
				}
			});
		}
	}

	async update(req, res){
		const name = req.body.name;
		const status = req.body.status;

		req.checkBody('name', 'El campo nombre no puede estar vacio').notEmpty();
		req.checkBody('status', 'El campo status no puede estar vacio').notEmpty();
		let errors = req.validationErrors();

		if(errors){
			return res.status(500).json({
				status: 500,
				errors
			});
		}
		else{
			var id = req.params.id;
			Country.findOne({_id: id}, function(error, countryDB){
				var current_date = Date.now();
				countryDB.name = name;
				countryDB.status = status;
				countryDB.updated_at = current_date;

				countryDB.save(function(error){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						});
					else{
						return res.json({
							status: 200,
							message: 'Cambios realizados exitosamente',
							country: CountryTransformer.transform(countryDB)
						});
					}
				});
			});
		}
	}

	async show(req, res){
		var id = req.params.id;
		Country.findOne({_id: id}, function(error, countryDB){
			if(error)
				return res.json(500).json({
					status: 500,
					error
				});
			else{
				return res.json({
					status: 200,
					country: CountryTransformer.transform(countryDB)
				});
			}
		});
	}

	async index(req, res){
		Country.find(function(error, countriesDB){
			if(error)
				return res.json(500).json({
					status: 500,
					error
				});
			else{
				return res.json({
					status: 200,
					message: 'Paises cargados exitosamente',
					countries: CountryTransformer.transform(countriesDB)
				});
			}

		});
	}

	async delete(req, res){
		var id = req.params.id;
		Country.findOne({_id: id}, function(error, countryDB){
			if(error)
				return res.status(400).send(error);
			else{
				var current_date = Date.now();
				countryDB.updated_at = current_date;
				countryDB.status = 2;
				countryDB.save(function(error, countryDBDeleted){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						});
					else
						return res.json({
							status: 200,
							message: 'Se ha eliminado el registro',
							country: CountryTransformer.transform(countryDBDeleted)
						});
				});
			}
		});
	}

}

module.exports = new CountryController();
