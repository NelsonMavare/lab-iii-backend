"use strict";
const Purchase = require('../models/Purchase');
const User = require('../models/User');	
const ActionPost = require('../models/ActionPost');	
const Post = require('../models/Post');
const UserProfile = require('../models/UserProfile');
const PurchaseTransformer = require("../transformers/PurchaseTransformer");


function findBuyer(user_profiles, user){
    var result = false
    user_profiles.map(user_profile => {
      if(user._id.toString() == user_profile.user.toString()){
        result = user_profile  
      }
    })

    return result
}

function findUser(user_profiles, user_id){
    var result = false
    user_profiles.map(user_profile => {
      if(user_id.toString() == user_profile.user.toString()){
        result = user_profile  
      }
    })

    return result
}

function getUserByPost(user_completes, user) {
	var result = false
  user_completes.map(user_complete => {
    if(user.toString() == user_complete._id.toString()){
      result = user_complete  
    }
  })

  return result
}

async function buyersByPurchase(purchasesDB){

    let buyer_ids = []		
    var user_completes = []

    purchasesDB.forEach((purchase) => {
      buyer_ids.push(purchase.buyer._id)
    })

    for (let i = 0; i < purchasesDB.length; i++) {
	    var user = await User.findById(purchasesDB[i].action_post.post.user)
    	var user_profile = await UserProfile.find({user: user._id });

    	var user_complete = Object.assign({}, user._doc, {
  			user_profile: Object.assign({}, user_profile[0]._doc) 
    	})

      user_completes.push(user_complete)
	  }


    var buyer_profiles = await UserProfile.find({user: {$in: buyer_ids} });

    purchasesDB = purchasesDB.map((purchase) => {
      if(findBuyer(buyer_profiles, purchase._doc.buyer) != false){
        var purchasenew = Object.assign({}, purchase._doc, {
          buyer: Object.assign({}, purchase._doc.buyer._doc, {
            buyer_profile: findBuyer(buyer_profiles, purchase._doc.buyer)
          }),
          action_post: Object.assign({}, purchase._doc.action_post._doc, {
          	post: Object.assign({}, purchase._doc.action_post._doc.post._doc, {
          		user: getUserByPost(user_completes, purchase._doc.action_post._doc.post._doc.user)
          	})
          })
        })
        return purchasenew
      }
      return purchase
    }) 

    return purchasesDB

}

class PurchaseController{
	
	constructor(){
	}

 
	/*ESTRUCTURA DEL JSON
	{
	"post": "5dc489faad5766256463694e",
	"action": "5db91c0a7fbcd30e7caf8300",
	"buyer": "5dbd87586733fe44147bef6a",
	"requirements": "Esto es una prueba bien chevere"
	}*/
	//CREA LA SOLICITUD DE ORDEN DE COMPRA PARA EL COMPRADOR
	async create(req, res){
		const post = req.body.post;
		const action = req.body.action;
		const buyer = req.body.buyer;
		const requirements = req.body.requirements;

		req.checkBody('requirements', 'El campo descripcion no puede estar vacio').notEmpty();
		req.checkBody('post', 'El id del post no puede estar vacio').notEmpty();
		req.checkBody('buyer', 'El id del comprador no puede estar vacio').notEmpty();
		req.checkBody('action', 'El id de la accion no puede estar vacio').notEmpty();
		let errors = req.validationErrors();

	    if(errors){
	      return res.status(500).json({
	        status: 500,
	        errors
	      });
	    }

	    //Check that user exists
	    User.findById(buyer, async function(error, userDB){
	    	if(error)
	    		return res.status(500).json({
	    			status: 500,
	    			error
	    		});
	    	if(!userDB)
	    		return res.json({
	    			status: 401,
	    			message: 'El usuario no existe'
	    		});
	    	else{

	    		//first create a new action post
	    		var newActionPost = new ActionPost({
	              action_date: Date.now(), 
	              post: post,
	              action: action
	            });

	            newActionPost.save(async function(error, actionPostDB){
	            	if(error)
	            		return res.status(500).json({
	            			status: 500,
	            			error
	            		});
	            	else{
	            		//create the new purchase associated with the action post
	            		var newPurchase = new Purchase({
	            			description: requirements,
	            			action_post: actionPostDB._id,
	            			buyer: userDB._id
	            		});

	            		//save the purchase
	            		var purchaseDB = await newPurchase.save();
	            		Purchase.findById(purchaseDB._id).populate('buyer')
	            		.populate({path: 'action_post', populate: {path: 'post', populate: {path: 'category'}}})
	            		.populate({path: 'action_post', populate: {path: 'action'}})
	            		.exec(async function(error, userPurchaseDB){
	            			if(error)
	            				return res.status(500).json({
	            					status: 500,
	            					error
	            				});
	            			else{

	            				var purchaseToSend = []
	            				purchaseToSend.push(userPurchaseDB)
	            				let userPurchaseDBToSend = await buyersByPurchase(purchaseToSend)

	            				return res.json({
	            					status: 200,
	            					message: 'Solicitud de compra realizada',
	            					purchase: PurchaseTransformer.transform(userPurchaseDBToSend)
	            				})
	            			}
	            		});
	            	}
	            });	
	    	}
	    });
	
	}

	//SOLICITUDES DE COMPRA DEL VENDEDOR
	//estados 1: solicitada, estado 2: aceptada, estado 3: rechazada,  estado 4: cancelada, estado 5: finalizada 
	/*ESTRUCTURA DEL JSON
	{
	"user": "5dbd87586733fe44147bef6a"
	}*/
	async seller_purchases(req, res){
		const user = req.body.user;

		req.checkBody('user', 'El id del usuario no puede estar vacia').notEmpty();
    	let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).json({
	      	status: 400,
	      	errors 
	      });
	    }

		//check if the user exists
		User.findById(user, async function(error, userDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			if(!userDB)
				return res.json({
					status: 401,
					message: 'El usuario no existe'
				});
			else{

				//get posts from user
				var postsDB = await Post.find({user: user});
				var postsArray = [];
				for(var i = 0; i < postsDB.length; i++){
					postsArray.push(postsDB[i]._id);
				}

				//get actionposts associated with post
				var actionPostsDB = await ActionPost.find({post: {$in: postsArray}});
				var actionPostsArray = [];
				for(var i = 0; i < actionPostsDB.length; i++){
					actionPostsArray.push(actionPostsDB[i]._id);
				}

				//get purchases from actionposts
				Purchase.find({action_post: {$in: actionPostsArray}})
					.populate('buyer')
	            	.populate({path: 'action_post', populate: {path: 'post', populate: {path: 'category'}}})
	            	.populate({path: 'action_post', populate: {path: 'action'}})
	            	.exec(async function(error, purchasesDB){
					if(error)
						return res.status(500).json({
							status: 500,
							error	
						});
					if(!purchasesDB)
						return res.json({
							status: 401,
							message: 'No se han encontrado solucitudes de compra',
						});
					else{
						purchasesDB = await buyersByPurchase(purchasesDB)
						return res.json({
							status: 200,
							message: 'Solicitudes de compra asociadas al vendedor',
							purchases: PurchaseTransformer.transform(purchasesDB)
						})
					}
				});
			}
		})
	}

	//SOLICITUDES DE COMPRA DEL COMPRADOR
	/*ESTRUCTURA DEL JSON
	{
	"user": "5dbd87586733fe44147bef6a"
	}*/
	async buyer_purchases(req, res){
		const user = req.body.user;

		req.checkBody('user', 'El id del usuario no puede estar vacia').notEmpty();
    	let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).json({
	      	status: 400,
	      	errors
	      });
	    }

		//check if user exists
		User.findById(user, async function(error, userDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			if(!userDB)
				return res.json({
					status: 401,
					message: 'El usuario no existe'
				});
			else{
				//HACER QUE EL USERPROFILE QUEDE DENTRO DEL USER X DIO
				Purchase.find({buyer: userDB._id})
					.populate('buyer')
            		.populate({path: 'action_post', populate: {path: 'post', populate: {path: 'category'}}})
            		.populate({path: 'action_post', populate: {path: 'action'}})
            		.exec(async function(error, purchasesDB){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						});
					if(!purchasesDB)
						return res.json({
							status: 401,
							message: 'El usuario no ha solicitado ninguna compra'
						});
					else{
						purchasesDB = await buyersByPurchase(purchasesDB)
						return res.json({
							status: 200,
							message: 'Solicitudes de compra del usuario',
							purchases: PurchaseTransformer.transform(purchasesDB)
						})
					}
				});
			}
		});
	} 

	//METODOS PARA CAMBIAR LOS ESTADOS DE LA SOLICITUD 

	//METODO PARA QUE EL VENDEDOR ACEPTE LA REQUEST DEL COMPRADOR CORRESPONDE AL ESTADO 2
	/*ESTRUCTURA DEL JSON
	{
	"purchase": "5dccd849e19087069826f9c2"
	}
	*/
	async accept_request(req, res){
		var purchase = req.body.purchase;

		req.checkBody('purchase', 'El id de la solicitud de compra no puede estar vacia').notEmpty();
    	let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).json({
	      	status: 400,
	      	errors
	      });
	    }

		Purchase.findById(purchase, async function(error, purchaseDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			if(!purchaseDB)
				return res.json({
					status: 401,
					message: 'La solicitud de compra no existe'
				});
			else{
				if(purchaseDB.status != 1)						
					return res.json({
						status: 400,
						message: 'El estado de la solicitud debe ser Solicitada'
					});

				else{
					purchaseDB.status = 2;
					var updatedPurchaseDB = await purchaseDB.save();

					Purchase.findById(updatedPurchaseDB._id)
						.populate('buyer')
			    		.populate({path: 'action_post', populate: {path: 'post', populate: {path: 'category'}}})
			    		.populate({path: 'action_post', populate: {path: 'action'}})
			    		.exec(async function(error, sendPurchase){
						if(error)
							return res.status(500).json({
								status: 500,
								error
							});
						else{
							var purchaseToSend = []
      				purchaseToSend.push(sendPurchase)
      				let userPurchaseDBToSend = await buyersByPurchase(purchaseToSend)
							return res.json({
								status: 200,
								message: 'Se ha aceptado la solicitud',
								purchase: PurchaseTransformer.transform(userPurchaseDBToSend[0])
							});
						}

					});

				}
			}
		});
	}

	//METODO PARA RECHAZAR SOLICITUD CORRESPONDE AL ESTADO 3
	/*ESTRUCTURA DEL JSON
	{
	"purchase": "5dce00ae7a707e3c80a508e1"
	}*/
	async reject_request(req, res){
		var purchase = req.body.purchase;

		req.checkBody('purchase', 'El id de la solicitud de compra no puede estar vacia').notEmpty();
    	let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).json({
	      	status: 400,
	      	errors
	      });
	    }

		Purchase.findById(purchase, async function(error, purchaseDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			if(!purchaseDB)
				return res.json({
					status: 401,
					message: 'La solicitud de compra no existe'
				});
			else{
				if(purchaseDB.status != 1)						
					return res.json({
						status: 400,
						message: 'El estado de la solicitud debe ser Solicitada'
					});

				else{
					purchaseDB.status = 3;
					var updatedPurchaseDB = await purchaseDB.save();

					Purchase.findById(updatedPurchaseDB._id)
						.populate('buyer')
			    		.populate({path: 'action_post', populate: {path: 'post', populate: {path: 'category'}}})
			    		.populate({path: 'action_post', populate: {path: 'action'}})
			    		.exec(async function(error, sendPurchase){
						if(error)
							return res.status(500).json({
								status: 500,
								error
							});
						else{
							var purchaseToSend = []
      				purchaseToSend.push(sendPurchase)
      				let userPurchaseDBToSend = await buyersByPurchase(purchaseToSend)
							return res.json({
								status: 200,
								message: 'Se ha rechazado la solicitud',
								purchase: PurchaseTransformer.transform(userPurchaseDBToSend[0])
							});
						}

					});

				}
			}
		});		
	}

	//METODO PARA CANCELAR UNA SOLICITUD DE COMPRA, CORRESPONDE AL AESTADO 4
	/*ESTRUCTURA DEL JSON
	{
	"purchase": "5dccd849e19087069826f9c2",
	"cancel_description": "Porque me dio la gana jajaja"
	}*/
	async cancel_request(req, res){
		var purchase = req.body.purchase;
		var cancel_description = req.body.cancel_description;

		req.checkBody('cancel_description', 'La razón de cancelación no puede estar vacía').notEmpty();
		req.checkBody('purchase', 'El id de la solicitud de compra no puede estar vacia').notEmpty();
    	let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).json({
	      	status: 400,
	      	errors
	      });
	    }

		Purchase.findById(purchase, async function(error, purchaseDB){
			console.log(purchaseDB);
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			if(!purchaseDB)
				return res.json({
					status: 401,
					message: 'La solicitud de compra no existe'
				});
			else{
				if(purchaseDB.status == 1 || purchaseDB.status == 2){
					purchaseDB.status = 4;
					purchaseDB.cancel_description = cancel_description;
					var updatedPurchaseDB = await purchaseDB.save();

					Purchase.findById(updatedPurchaseDB._id)
						.populate('buyer')
			    		.populate({path: 'action_post', populate: {path: 'post', populate: {path: 'category'}}})
			    		.populate({path: 'action_post', populate: {path: 'action'}})
			    		.exec(async function(error, sendPurchase){
						if(error)
							return res.status(500).json({
								status: 500,
								error
							});
						else{

							console.log(sendPurchase)
							var purchaseToSend = []
      				purchaseToSend.push(sendPurchase)
      				let userPurchaseDBToSend = await buyersByPurchase(purchaseToSend)
      				
							return res.json({
								status: 200,
								message: 'Se ha cancelado la solicitud',
								purchase: PurchaseTransformer.transform(userPurchaseDBToSend[0])
							});
						}

					});						
				}						

				else{
					return res.json({
							status: 400,
							message: 'El estado de la solicitud debe ser Solicitada o Aceptada'
					});
				}
			}
		});
	}

	//FALTA ESTE METODO PARA CAMBIAR EL ESTADO A 5 QUE ES FINALIZADO DONDE EL VENDEDOR SUBA LAS IMAGENES
	//METODO PARA FINALIZAR COMPRA 
	/* WITH FORM DATA APPEND
		purchase: { 
			"purchase_id": "5dd61ecae80e911975f1793f"
		},
		image1: "imagen.png",
		image2: "imagen2.png",
		...
	*/
	/* WITH INDIVIDUAL VALUES
		purchase_id: "5dd61ecae80e911975f1793f"
		image1: "imagen.png",
		image2: "imagen2.png",
		...
	*/

	 downloadFileImage(req, res){
		console.log(req.params.id)
		res.download('./public/uploads/purchases/'+req.params.id,req.params.id,function(err){
			if(err) console.log(err)
			else console.log("listo")
		});
		}

	async finish_purchase_request(req, res){

		if(req.body.purchase){
			req.body = JSON.parse(req.body.purchase); 
		}

		var purchase = req.body.purchase_id

		console.log(purchase)

		if(!purchase)
			return res.json({
				status: 500,
				message: 'La solicitud de compra es necesaria.'
			})
	    
		Purchase.findById(purchase)
						.populate('buyer')
		    		.populate({path: 'action_post', populate: {path: 'post', populate: {path: 'category'}}})
		    		.populate({path: 'action_post', populate: {path: 'action'}}).exec( async function(error, purchaseDB){

			console.log(purchaseDB);

			if(error)
				return res.status(500).json({
					status: 500,
					error
				});

			if(!purchaseDB)
				return res.json({
					status: 401,
					message: 'La solicitud de compra no existe'
				});


			if(purchaseDB.status == 2){

				var images_length = 0

		    if(req.files != undefined){
		      images_length = Object.keys(req.files).length
		    }

		    if(!req.files || images_length === 0){
			    return res.json({
			      status: 500,
			      message: 'Debes subir al menos un archivo'
			    })
			  }

	      let new_image_name = null

	      for(var i in req.files){
		      var image = req.files[i];
		      let allowed_extensions = ['jpg', 'jpeg', 'png'];
		      let short_name = image.name.split('.');
		      let extension = short_name[short_name.length -1];

		      // Verify extension allowed
		      if(allowed_extensions.indexOf(extension) < 0){
		        return res.json({
		          status: 500,
		          message: 'Formato de archivo no válido, las extensiones permitidas son ' + allowed_extensions.join(', ')
		        })
		      }

		      new_image_name =`${new Date().getMilliseconds()}.${extension}`;

		      //upload
		      image.mv('public/uploads/purchases/'+new_image_name, (err) => {
		        if (err){
		          var pathImage = path.resolve(__dirname, `../public/uploads/purchases/${new_image_name}`);
		          if( fs.existsSync(pathImage) ){
		            fs.unlinkSync(pathImage);
		          }
		          return res.status(500).json({
		            status: 500,
		            err
		          });
		        }
		      });
		      purchaseDB.pictures.push(new_image_name);
		    }
		    purchaseDB.status = 5

		    let purchaseArray = []
		    let purchaseUpdated = await purchaseDB.save();

		    purchaseArray.push(purchaseUpdated);

		    let purchaseToSend = await buyersByPurchase(purchaseArray)
		    
		    return res.json({
		    	status: 200,
		    	message: 'La compra ha finalizado y se han subido los archivos exitosamente',
		    	purchase: PurchaseTransformer.transform(purchaseToSend[0])
		    })

			}else{
				
				if(purchaseDB.status === 5){
					return res.json({
							status: 400,
							message: 'La compra ya ha finalizado.'
					});
				}

				return res.json({
						status: 400,
						message: 'El estado de la solicitud debe ser Aceptada'
				});
			}						
		});		
	}


	//METODO PARA CAMBIAR UN ESTADO AL QUE SEA
	/*ESTRUCTURA DEL JSON
	{
	"purchase": "5dccd849e19087069826f9c2",
	"status": 2
	}
	
	async evaluate_buyer_request(req, res){
		var purchase = req.body.purchase;
		var status = req.body.status;

		req.checkBody('status', 'El estado es necesario').notEmpty();
		req.checkBody('purchase', 'El id de la solicitud de compra no puede estar vacia').notEmpty();
    	let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).json({
	      	status: 400,
	      	errors
	      });
	    }

		Purchase.findOneAndUpdate({_id: purchase}, {status}, {new: true})
			.populate('buyer')
    		.populate({path: 'action_post', populate: {path: 'post'}})
    		.populate({path: 'action_post', populate: {path: 'action'}})
    		.exec(function(error, purchaseDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			if(!purchaseDB)
				return res.json({
					status: 401,
					message: 'La solicitud de compra no existe'
				});
			else
				return res.json({
					status: 200,
					message: 'Estado de la solicitud actualizado',
					purchase: PurchaseTransformer.transform(purchaseDB)
				});
		});
	} */
}

module.exports = new PurchaseController();
