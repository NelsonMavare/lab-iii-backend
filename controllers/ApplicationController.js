"use strict";
const User = require('../models/User');
const Role = require('../models/Role');
const bcrypt = require('bcryptjs');
const service = require('../config/services');
const ApplicationTransformer = require("../transformers/ApplicationTransformer");
const UserProfile = require('../models/UserProfile');

const Post = require('../models/Post');
const Action = require('../models/Action');
const ActionPost = require('../models/ActionPost');
const Purchase = require('../models/Purchase');
const Comment = require('../models/Comment');
const CommentTransformer = require('../transformers/CommentTransformer');

const generator = require('generate-password');
const nodemailer = require('nodemailer');

class ApplicationController {
  constructor() {
    this.login = this.login.bind(this);
    this.register = this.register.bind(this);
    this.addDays = this.addDays.bind(this);
    this.forgot_password = this.forgot_password.bind(this);
  }

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  /*
  {
    "email": "jejoalca16@gmail.com",
    "password": "123456"
  }
  */

  async login(req, res){
    const email = req.body.email;
    const password = req.body.password;

    User.findOne({email: email}, function(error, user){

      if(error)
        throw error;
      if(!user){
        return res.status(401).json({message: 'Usuario no encontrado'});
      }

      //see if password is correct
      bcrypt.compare(password, user.password, function(error, isMatch){
        if(error)
          throw error;
        if(isMatch){

          //update last login
          user.last_login_at = Date.now();
          user.save(function(error){
            if(error){
              return res.status(500).send(error);
            }
          });
          
          UserProfile.findOne({user: user._id}).populate('user').populate('country').exec( async function(error, userProfileDB){
            if(error)
              return res.status(400).json({
                status: 400,
                error
              });
            else{
              
              let user = await User.findById(userProfileDB.user._id).populate('role').exec();
              
              userProfileDB = Object.assign({}, userProfileDB._doc, {
                role: user.role
              })

              return res.json({
                status: 200,
                user: ApplicationTransformer.transform(userProfileDB),
                token: user.token
              });
            }
          });
        }
        else{
          return res.status(401).json({message:'Contraseña incorrecta'});
        }
      });
    });
  }

  /*
  {
    "email": "jejoalca16@gmail.com",
    "password": "123456",
    "passwordConfirm": "123456",
    "country": "5db05d1c0eff1420e7b4ec99"
  }
  */

  async register(req, res){
    const email = req.body.email;
    const password = req.body.password;
    const passwordConfirm = req.body.confirmPassword;

    //validate request un
    req.checkBody('country', 'Pais requerido').notEmpty();
    req.checkBody('email', 'Email requerido').notEmpty();
    req.checkBody('password', 'Contraseña requerida').notEmpty();
    req.checkBody('passwordConfirm', 'Confirme contraseña').notEmpty();  
    req.checkBody('passwordConfirm', 'Contraseñas no son iguales').equals(req.body.password);
    req.checkBody('email', 'Email no posee un formato válido').isEmail();
    let errors = req.validationErrors();

    //check for errors
    if(errors){
      return res.send(errors);
    }

    //check if user already exists
    else{
      User.findOne({email: email}, async function(error, user){
        if(user){
          return res.send({message: 'Ya hay un usuario registrado con ese email'});
        }
        //if it doesn't exist
        else{
          var current_date = Date.now();
          var result = new Date(current_date);
          result.setDate(result.getDate() + 60);
          var status = 1;
          let role = await Role.findOne({name: 'Usuario'})

          let newUser = new User({
            email: email,
            password: password,
            status: status,
            token: service.createToken(user),
            created_at: current_date,
            updated_at: current_date,
            token_expiration_date: result,
            role: role._id
          });
          
          //Hash the password
          bcrypt.genSalt(10, function(error, salt){
            bcrypt.hash(newUser.password, salt, function(error, hash){
              if(error){
                return res.status(500).send(error);
              }
              newUser.password = hash;
              newUser.save(function(error, savedUser){;
                if(error){
                  return res.status(500).send(error)
                }
                else{
                  var userProfile = new UserProfile({
                    user: savedUser._id,
                    country: req.body.country,
                    created_at: Date.now(),
                    updated_at: Date.now()
                  });

                  userProfile.save( async function(error, userProfileDB){
                    if(error)
                      return res.status(500).json({
                        status: 500, 
                        error
                      })

                    UserProfile.findOne({user: savedUser._id}).populate('user').populate('country').exec( async function(error, userProfileDB){
                      if(error)
                        return res.status(400).json({
                          status: 400,
                          error
                        });
                      else{
                        
                        let user = await User.findById(userProfileDB.user._id).populate('role').exec();
                        
                        userProfileDB = Object.assign({}, userProfileDB._doc, {
                          role: user.role
                        })

                        return res.json({
                          status: 200,
                          user: ApplicationTransformer.transform(userProfileDB)
                        });
                      }
                    });
                  });
                }
              });
            });
          });
        }
      });
    }
  }

  async forgot_password(req, res){
    var email =  req.body.email;
    req.checkBody('email', 'Email no posee un formato válido').isEmail();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).send(errors);
    }

    //check if user already exists
    User.findOne({email: email}, function(error, user){
      if(!user)
        return res.status(401).send({message: 'Usuario no encontrado'});

      //set new password to user
      else{

        var newPassword =  generator.generate({length: 10, numbers: true});
        //encrypting the password
        bcrypt.genSalt(10, function(error, salt){
          bcrypt.hash(newPassword, salt, function(error, hash){
            if(error){
              return res.status(500).send(error);
            }
            /*var expiration_date = addDays(Date.now(), 60);
            var token = service.createToken(user);*/

            user.password = hash;
            user.updated_at = Date.now();
            //user.token = token;
            //user.token_expiration_date = expiration_date;

            //save new password for user
            user.save(function(error){
              if(error){
                return res.status(500).send(error);
              }
              console.log(user);
            });
          });
        });

        //config mail
        var smtpTrans = nodemailer.createTransport({
          service: 'SendGrid',
          auth: {
            user: 'mcrServicesTest',
            pass: 'AbC123aBc'
          },
          tls: {
                rejectUnauthorized: false
            }
        })

        var mailOptions = {
          to: user.email,
          from: 'proyectoLabiii_veronicaAPP@outlook.com', //change this
          subject: 'Password Reset',
          text: 'Has recibido este correo porque alguien ha solicitado un cambio de contraseña a la cuenta asociada.\n\n' +
          'La nueva contraseña es: ' + newPassword +'\n' 
        };
        smtpTrans.sendMail(mailOptions, function(error) {
          if(error){
            console.log(error);
            return res.status(500).send({message: 'No se pudo enviar el email'});
          }
          else
            return res.status(200).send({message: 'Se ha enviado una nueva contraseña a: ' + user.email});
        });

      }
    });
  }

  //PROMEDIO DE LA CALIFICACION DEL USUARIO 
  async average(req, res){
    var user = req.body.user;
    var email = req.body.email;

    req.checkBody('user', 'El email no puede estar vacio').notEmpty();
    let errors = req.validationErrors();

      if(errors){
        return res.status(500).json({
          status: 500,
          errors
        });
      } 

      User.findOne({email: email}, async function(error, userDB){
        if(error)
          return res.status(500).json({
            status: 500,
            error
          });
        if(!userDB)
          return res.json({
            status: 401,
            message: 'El usuario no existe'
          });
        else{
            //get if of user's posts
            var postsDB = await Post.find({user: userDB._id});
            console.log(postsDB);
            var postsArray = [];
            for(var i = 0; i < postsDB.length; i++){
               postsArray.push(postsDB[i]._id);
            }

            //get id of action buy
            var actionDB = await Action.findOne({name: 'Me gusta'});

            //get actionposts 
            var actionPostsDB = await ActionPost.find({action: actionDB._id, post: {$in: postsArray}});
            //array of actionposts ids
            var actionPostsArray = [];
            for(var i = 0; i < actionPostsDB.length; i++){
               actionPostsArray.push(actionPostsDB[i]._id);
            }

            //get purchases
            var purchasesDB = await Purchase.find({action_post: {$in: actionPostsArray}});

            //calculate average score
            var acum_score = 0;
            var con_score = 0;
            for(var i = 0; i < purchasesDB.length; i++){
              if(purchasesDB[i].status == 5){
                acum_score = purchasesDB[i].score;
                con_score ++;
              }
            }

            //check that there's at least one score
            if(con_score == 0)
              return res.json({
                status: 400,
                message: 'El usuario no ha recibido ninguna calificacion'
              })
            else{
              return res.json({
                status: 200,
                message: 'Promedio del usuario',
                average_score: acum_score/con_score
              })
            }

        }

      });
  }

}

module.exports = new ApplicationController();
