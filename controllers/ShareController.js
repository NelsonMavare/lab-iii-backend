"use strict";
const User = require('../models/User');
const ApplicationTransformer = require("../transformers/ApplicationTransformer");

const nodemailer = require('nodemailer');

class ApplicationController {
  constructor() {
    this.share_post = this.share_post.bind(this);
  }

  async share_post(req, res){
    var email =  req.body.email;
    req.checkBody('email', 'Email is not valid').isEmail();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).send(errors);
    }

    //check if user already exists
    User.findOne({email: email}, function(error, user){
      if(!user)
        return res.status(401).send({message: 'User not found'});

      //set new password to user
      else{
      	var link = 'aqui va el link';

        //config mail
        var smtpTrans = nodemailer.createTransport({
          service: 'SendGrid',
          auth: {
            user: 'mcrServicesTest',
            pass: 'AbC123aBc'
          },
          tls: {
                rejectUnauthorized: false
            }
        })

        var mailOptions = {
          to: user.email,
          from: 'proyectoLabiii_veronicaAPP@outlook.com', //change this
          subject: 'Shared post',
          text: 'Has recibido este correo porque alguien ha decidido compartir una publicación con usted.\n\n' +
          'Link de la publicación: ' + link +'\n' 
        };
        smtpTrans.sendMail(mailOptions, function(error) {
          if(error){
            console.log(error);
            return res.status(500).send({message: 'No se pudo enviar el email'});
          }
          else
            return res.status(200).send({message: 'Se ha enviado el link del post a: ' + user.email});
        });

      }
    });
  }

}

module.exports = new ApplicationController();