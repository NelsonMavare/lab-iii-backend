'use-strict'
const Action = require('../models/Action');
const ActionTransformer = require('../transformers/ActionTransformer')

class ActionController {
	
  constructor() {
    this.index = this.index.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.show = this.show.bind(this);
  }

  async index(req, res, next){

  	Action.find({}, (err, actionsDB) => {
  		if(err)
  			return res.json({
  				status: 500,
  				err
  			})

  		return res.json({
  			status: 200,
  			message: 'Acciones cargadas con éxito',
  			actions: ActionTransformer.transform(actionsDB)
  		})

  	})

  }

  async create(req, res, next){
  	const {name, description} = req.body

  	req.checkBody('name', 'El nombre es necesario').notEmpty();
    req.checkBody('description', 'La descripción es necesaria').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).json({
      	status: 400,
      	errors
      });
    }

    Action.findOne({name}, (e, actionFoundDB) => {
    	if(e)
    		return res.status(500).json({
    			status: 500,
    			e
    		}) 

    	if(actionFoundDB)
    		return res.json({
    			status: 500,
    			message: 'Ya existe una acción con ese nombre'
    		})

    	let action = new Action({name, description})

    	action.save((err, actionDB) => {
	    	
	    	if(err)
	    		return res.status(500).json({
	    			status: 500,
	    			err
	    		})

	    	return res.json({
	    		status: 200, 
	    		message: 'Acción creada con éxito',
	    		action: ActionTransformer.transform(actionDB)
	    	})

    	})

    })

  }

  async show(req, res, next){
  	const {id} = req.params

  	if(!id)
  		return res.json({
  			status: 400,
  			message: 'Ingrese un id'
  		})

  	Action.findById(id, (err, actionDB) => {

  		if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

  		if(!actionDB)
  			return res.json({
  				status: 401,
  				message: 'La acción no existe'
  			})

  		return res.json({
  			status: 200,
  			message: 'Acción encontrada',
  			action: ActionTransformer.transform(actionDB)
  		})

  	})
  }

  async update(req, res, nex){
  	const {id} = req.params
  	const {name, description, status} = req.body

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

  	req.checkBody('name', 'El nombre es necesario').notEmpty();
    req.checkBody('description', 'La descripción es necesaria').notEmpty();
    req.checkBody('status', 'El estatus no puede estar vacio').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).json({
      	status: 400,
      	errors
      });
    }

    Action.findByIdAndUpdate(id, {name, description, status}, {new: true}, (err, actionDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

    	if(!actionDB)
    		return res.json({
    			status: 401,
    			message: 'La acción no existe'
    		})

    	return res.json({
    		status: 200,
    		message: 'Datos de la acción modificados exitosamente',
    		action: actionDB
    	})

    })
  }

  async delete(req, res, next){
  	const {id} = req.params

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

    let oldAction = await Action.findById(id);

    if(!oldAction)
      return res.json({
        status: 401,
        message: 'La acción no existe'
      })


    if(oldAction.status === 2)
      return res.json({
        status: 401,
        message: 'La acción ya se encuentra inactiva'
      })   

    Action.findByIdAndUpdate(id, {status: 2}, (err, actionDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

    	return res.json({
    		status: 200,
    		message: 'Acción eliminada exitosamente',
    		action: actionDB
    	})

    })

  }

  async reactivate(req, res, next){
  	const {id} = req.params

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

    let oldAction = await Action.findById(id);

    if(!oldAction)
      return res.json({
        status: 401,
        message: 'La acción no existe'
      })


    if(oldAction.status === 1)
      return res.json({
        status: 401,
        message: 'La acción ya se encuentra activa'
      })   

    Action.findByIdAndUpdate(id, {status: 1}, {new: true}, (err, actionDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

    	return res.json({
    		status: 200,
    		message: 'Acción reactivada exitosamente',
    		action: actionDB
    	})

    })
  }

}

module.exports = new ActionController();