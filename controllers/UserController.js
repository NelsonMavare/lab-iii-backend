"use strict";
const User = require('../models/User');
const Role = require('../models/Role');
const UserProfile  = require('../models/UserProfile');
const Country = require('../models/Country');
const bcrypt = require('bcryptjs');
const service = require('../config/services');
const UserTransformer = require("../transformers/UserTransformer");
const path = require('path');
const fs = require('fs');
const Post = require('../models/Post');
const Purchase = require('../models/Purchase');
const ActionPost = require('../models/ActionPost');
const Action = require('../models/Action');


  function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

class UserController {
  
  constructor() {
    this.UserTransformer = UserTransformer;
    this.changePassword = this.changePassword.bind(this);

    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.show = this.show.bind(this);
    this.index = this.index.bind(this);
    this.delete = this.delete.bind(this);
    this.exists = this.exists.bind(this);
    this.total_gain = this.total_gain.bind(this);
    this.weeks = this.weeks.bind(this);
  }


  //Verificar si usuario existe por id (body)

  /*
    body: {
      user: "5dccdab1b9593b3760c9d3aa"
    }
  */

  async existsById(req, res, next){
    const {user} = req.body

    req.checkBody('user', 'Usuario requerido').notEmpty();

    let errors = req.validationErrors();

    //check for errors
    if(errors){
      return res.status(500).json({
        status: 500,
        errors
      });
    }

    if(!user)
      return res.json({
        status: 400,
        message: 'Ingrese un id del usuario'
      })

    User.findById(user, (err, userDB) => {

      if(err)
        return res.status(500).json({
          status: 500,
          err
        })

      if(!userDB)
        return res.json({
          status: 401,
          message: 'El usuario no existe'
        })

      console.log('user', userDB)

      next();
    })   
  }

  async exists(req, res, next){
    const id = req.params.id

    if(!id)
      return res.json({
        status: 400,
        message: 'Ingrese un id del usuario'
      })

    User.findById(id, (err, userDB) => {

      if(err)
        return res.status(500).json({
          status: 500,
          err
        })

      if(!userDB)
        return res.json({
          status: 401,
          message: 'El usuario no existe'
        })

      console.log('user', userDB)

      next();
    })    

  }

  /*
  body {
    email: 'jejoalca2120@gmail.com',
    password: '123456',
    passwordConfirm: '123456',
    country: '5db05d1c0eff1420e7b4ec99',
    telephone: '04245558208',
    role: '5dcc64203f614841a01b0958',
    birthday: '1996/07/30',
    gender: '1',
    name: 'jeferson',
    surname: 'alvarado'
  }
  */

  async create(req, res){
    //user
    const email = req.body.email;
    const password = req.body.password;
    const passwordConfirm = req.body.confirmPassword;

    //user profile
    const name = req.body.name;
    const surname = req.body.surname;
    const birthday = req.body.birthday;
    const gender = req.body.gender;
    const telephone = req.body.telephone;
    const facebook = req.body.facebook;
    const twitter = req.body.twitter;
    const instagram = req.body.instagram;
    const country = req.body.country;
    const role = req.body.role;

    //validate request un
    req.checkBody('telephone', 'Teléfono requerido').notEmpty();
    req.checkBody('country', 'Country requerido').notEmpty();
    req.checkBody('role', 'Role requerido').notEmpty();
    req.checkBody('email', 'Email requerido').notEmpty();
    req.checkBody('password', 'Contraseña requerida').notEmpty();
    req.checkBody('passwordConfirm', 'Confirme contraseña').notEmpty();  
    req.checkBody('passwordConfirm', 'Contraseñas no son iguales').equals(req.body.password);
    req.checkBody('email', 'Email inválido').isEmail();

    //userprofile validations
    req.checkBody('name', 'Nombre requerido').notEmpty();
    req.checkBody('surname', 'Apellido requerido').notEmpty();
    req.checkBody('birthday', 'Fecha de nacimiento requerida').notEmpty();
    req.checkBody('gender', 'Género requerido').notEmpty();

    let errors = req.validationErrors();

    //check for errors
    if(errors){
      return res.status(500).json({
        status: 500,
        errors
      });
    }

    //check if user already exists
    else{
      User.findOne({email: email}, async function(error, user){
        if(user){
          return res.json({
            status:500,
            message: 'User already registered with that email'
          });
        }
        //if it doesn't exist
        else{
          var current_date = Date.now();
          var result = new Date(current_date);
          result.setDate(result.getDate() + 60);
          let role_find = await Role.findById(role)

          if(!role_find)
            return res.json({
              status: 500,
              message: 'El rol no se encuentra registrado'
            })

          let newUser = new User({
            email: email,
            password: password,
            token: service.createToken(user),
            created_at: current_date,
            updated_at: current_date,
            token_expiration_date: result,
            role: role
          });

          //Hash the password
          bcrypt.genSalt(10, function(error, salt){
            bcrypt.hash(newUser.password, salt, function(error, hash){
              if(error){
                return res.status(500).json({
                  status: 500,
                  error
                });
              }
              newUser.password = hash;
              //save new user
              newUser.save(function(error, userDB){
                if(error){
                  return res.status(500).json({
                    status: 500,
                    error
                  })
                }

                else{
                  //save user profile
                  let userProfile = new UserProfile({
                    country: country,
                    user: userDB._id,
                    name: name,
                    surname: surname,
                    birthday: birthday,
                    gender: gender,
                    telephone: telephone,
                    facebook: facebook,
                    instagram: instagram,
                    twitter: twitter,
                    created_at: current_date,
                    updated_at: current_date,
                  });

                  userProfile.save(function(error, userProfileDB){
                    if(error)
                      return res.status(500).json({
                        status: 500,
                        error
                      });
                    else{
                      UserProfile.findOne({user: userDB._id}).populate('user').populate('country').exec( async function(error, userProfileDB){
                        if(error)
                          return res.status(400).json({
                            status: 400,
                            error
                          });
                        else{
                          
                          let user = await User.findById(userProfileDB.user._id).populate('role').exec();
                          
                          userProfileDB = Object.assign({}, userProfileDB._doc, {
                            role: user.role
                          })

                          console.log(userProfileDB)

                          return res.json({
                            status: 200,
                            user: UserTransformer.transform(userProfileDB)
                          });
                        }
                      });
                    }  
                  });
                }
              });
            });
          });
        }
      });
    }
  }

  /*
    {
      "name": "",
      "surname": "",
      "birthdate": "",
      "gender": ""
    }
  */
  async update(req, res){

    //user profile
    const name = req.body.name;
    const surname = req.body.surname;
    const birthday = req.body.birthday;
    const gender = req.body.gender;
    const telephone = req.body.telephone;
    const facebook = req.body.facebook;
    const twitter = req.body.twitter;
    const instagram = req.body.instagram;
    const country = req.body.country;
    const status = req.body.status;

    
    const {id} = req.params;
    UserProfile.findOne({user: id}, function(error, userProfileDB){
      if(error)
        return res.status(500).json({
          status:500,
          errors
        });

      if(!userProfileDB){
        return res.json({
          status: 401,
          message: 'El usuario no existe'
        })
      };

      var current_date = Date.now();

      //profile
      userProfileDB.name = name;
      userProfileDB.surname = surname;
      userProfileDB.birthday = birthday;
      userProfileDB.gender = gender;
      userProfileDB.telephone = telephone;
      userProfileDB.facebook = facebook;
      userProfileDB.twitter = twitter;
      userProfileDB.instagram = instagram;
      userProfileDB.country = country;
      userProfileDB.status = status;
      userProfileDB.updated_at = current_date;

      //updates user status
      User.findOne({_id: id}, function(error, user){
        if(error)
          return res.status(400).json({
            status:400,
            error
          });
        else{
          user.status = status;
          user.updated_at = current_date;
          user.save(function(error){
            if(error)
              return res.status(400).json({
                status: 400,
                error
              });
          });
        }
      });

      userProfileDB.save(function(error, userProfileDBFound){
        if(error)
          return res.status(400).json({
            status: 400,
            error
          });
        else{
            UserProfile.findOne({user: id}).populate('country').populate('user').exec(async function(error, userProfileDBFound){
              
              let user = await User.findById(userProfileDBFound.user._id).populate('role').exec();
              userProfileDBFound = Object.assign({}, userProfileDBFound._doc, {
                role: user.role
              })
              console.log(userProfileDBFound)
              return res.json({
                status: 200,
                message: 'Datos del usuario actualizados correctamente.',
                user: UserTransformer.transform(userProfileDBFound)
              });
            });
          }
      });
    });
  }

  async show(req, res){
    var id = req.params.id;

    UserProfile.findOne({user: id}).populate('user').populate('country').exec( async function(error, userProfileDB){
      if(error)
        return res.status(400).json({
          status: 400,
          error
        });
      else{
        let user = await User.findById(userProfileDB.user._id).populate('role').exec();
        userProfileDB = Object.assign({}, userProfileDB._doc, {
          role: user.role
        })
        return res.json({
          status: 200,
          user: UserTransformer.transform(userProfileDB)
        });
      }
    });
  }

  async index(req, res){
    UserProfile.find({}).populate({path: 'user', populate: {path: 'role'}}).populate('country').exec(function(error, usersProfilesDB){
      if(error)
        return res.status(400).json({
          status: 400,
          error
        });

      usersProfilesDB = usersProfilesDB.map((userProfileDB) => {
        return Object.assign({}, userProfileDB._doc, {
          role: userProfileDB.user.role
        })
      }) 
      
      console.log(usersProfilesDB)

      return res.json({
        status: 200,
        users: UserTransformer.transform(usersProfilesDB)
      });
    });
  }

  async delete(req, res){
    var id = req.params.id;

    let oldUser = await User.findById(id)

    if(!oldUser)
      return res.json({
        status: 401,
        message: 'El usuario no existe'
      })

    if(oldUser.status === 2)
      return res.json({
        status: 500,
        message: 'El usuario ya se encuentra inactivo'
      })   


    UserProfile.findOne({user: id}, function(error, userProfileDB){
      if(error)
        return res.status(400).json({
          status: 400,
          error
        });

      else{
        var current_date = Date.now();
        userProfileDB.updated_at = current_date;
        userProfileDB.status = 2;

        User.findOne({_id: id}, function(error, user){
          if(error)
            return res.status(400).json({
              status: 400,
              error
            });
          else{
            user.status = 2;
            user.updated_at = current_date;
            user.save(function(error){
              if(error)
                return res.status(400).json({
                  status: 400,
                  error
                });
            });
          }
        });

        userProfileDB.save(function(error){
          if(error)
            return res.status(400).json({
              status: 400,
              error
            });
          else{
            UserProfile.findOne({user: userProfileDB.user}).populate('country').populate('user').exec( async function(error, userProfileDBFound){
              
              let user = await User.findById(userProfileDBFound.user._id).populate('role').exec();
              userProfileDBFound = Object.assign({}, userProfileDBFound._doc, {
                role: user.role
              })

              return res.json({
                status: 200,
                message: 'Usuario eliminado exitosamente',
                user: UserTransformer.transform(userProfileDBFound)
              });

            });
          }
        });
      }
    });
  }

  /*
    {
      "passord": "",
      "newPassword": "",
      "passwordConfirm": "",
      "user_id": ""
    }
  */

  async changePassword(req, res){

    const password = req.body.password;
    const newPassword = req.body.newPassword;
    const passwordConfirm = req.body.confirmPassword;
    const user = req.body.user_id;

    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('user_id', 'User id is required').notEmpty();
    req.checkBody('passwordConfirm', 'Confirm password').notEmpty();
    req.checkBody('newPassword', 'New password is required').notEmpty();  
    req.checkBody('passwordConfirm', 'Password doesnt match').equals(req.body.newPassword);
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).json({
        status: 400,
        errors
      });
    }
    User.findById(user, function(error, user){

      bcrypt.compare(password, user.password, function(error, isMatch){

        if(!isMatch){
          return res.json({
            status: 500,
            message: 'Contraseña incorrecta'
          })
        }

        user.password = newPassword;

        bcrypt.genSalt(10, function(error, salt){
          bcrypt.hash(user.password, salt, function(error, hash){
            if(error){
              return res.status(500).json({
                status: 500,
                error
              });
            }
            user.updated_at = Date.now();
            user.password = hash;
            user.save(function(error){
              if(error){
                return res.status(500).json({
                  status: 500,
                  error
                });
              }
              else{
                res.json({
                  status: 200,
                  message:'Contraseña cambiada satisfactoriamente.'});
              }
            });
          })
        });
      })

    });
  }


  async reactivate(req, res, next){
    const {id} = req.params

    if(!id)
      return res.json({
        status: 400,
        message: 'Ingrese un id'
      })

    let oldUser = await User.findById(id)

    if(!oldUser)
      return res.json({
        status: 401,
        message: 'El usuario no existe'
      })

    if(oldUser.status === 1)
      return res.json({
        status: 500,
        message: 'El usuario ya se encuentra activo'
      })   

    User.findByIdAndUpdate(id, {status: 1}, {new: true}, (err, userDB) => {

      if(err)
          return res.status(500).json({
            status: 500,
            err
          });

      UserProfile.findOneAndUpdate({user: userDB._id}, {status: 1}, {new: true}).populate('country').populate('user').exec( async function(error, userProfileDB){
        if(error)
          return res.status(500).json({
            status: 500,
            error
          });
        
        let user = await User.findById(userProfileDB.user._id).populate('role').exec();
        userProfileDB = Object.assign({}, userProfileDB._doc, {
          role: user.role
        })

        return res.json({
          status: 200,
          message: 'Usuario reactivado con éxito.',
          user: UserTransformer.transform(userProfileDB)
        });
      }); 
    })
  }

   async uploadProfileImage(req, res, next) {

    const id = req.params.id

    if (!req.files)
      return res.status(400).json({
        status: 400,
        message: 'No se ha seleccionado ningún archivo'
      });
   
    let image = req.files.image;
    let allowed_extensions = ['jpg', 'jpeg', 'png', 'gif'];
    let short_name = image.name.split('.');
    let extension = short_name[short_name.length -1];

    if(allowed_extensions.indexOf(extension) < 0){
      return res.json({
        status: 500,
        message: 'Formato de archivo no válido, las extensiones permitidas son ' + allowed_extensions.join(', ')
      })
    }

    let user_profile = await UserProfile.findOne({user: id})
    let image_name = user_profile.image
    let pathImage = path.resolve(__dirname, `../public/uploads/users/${image_name}`);
    if( fs.existsSync(pathImage) ){
      fs.unlinkSync(pathImage);
    }

    let new_image_name = `${req.params.id}-${new Date().getMilliseconds()}.${extension}`;

    image.mv('public/uploads/users/'+new_image_name, (err) => {

      if (err)
        return res.status(500).json({
          status: 500,
          err
        });

        user_profile.image = new_image_name
        user_profile.save((err, result) => {
          if(err)
            return res.status(500).json({
              status: 500,
              err
            })

            return res.status(200).json({
              status: 200,
              message: 'Se ha subido con exito la imágen',
              image: new_image_name
            });

        })
    });
  } 

  async weeks(month, postsDB, actionDB){
    var start = new Date(2019, month, 1);
    var end = new Date(2019, month, 8);
    var weeks_array = [];

    for(var x=1; x<=4; x++){
      var weekly_total = 0;
      for(var y=0; y<postsDB.length; y++){
        //action posts 
        var actionPostsDB = await ActionPost.find({post: postsDB[y]._id, action: actionDB._id, status: 1}); 
        var actionPostsDB_array = [];
        for(var z=0; z<actionPostsDB.length; z++)
          actionPostsDB_array.push(actionPostsDB[z]._id);

        //get purchases 
        var purchasesDB = await Purchase.find({action_post: {$in: actionPostsDB_array}, status: {$in: [5,6]}, date: {"$gte": start, "$lt": end}});
        weekly_total += postsDB[y].price*purchasesDB.length;
      }
      var data = {
        week: x,
        weekly_total: weekly_total
      };
      weeks_array.push(data);
      start = addDays(start, 7);
      end = addDays(end, 7);
    }

    return weeks_array;
  }

  /*FORMATO DEL JSON DE RESPUESTA
      "months": [
        {
            "month": 1,
            "monthly_total": 0,
            "weeks": [
                {
                    "week": 1,
                    "weekly_total": 0
                },
                {
                    "week": 2,
                    "weekly_total": 0
                },
                {
                    "week": 3,
                    "weekly_total": 0
                },
                {
                    "week": 4,
                    "weekly_total": 0
                }
            ]
        }, 
  ASI SUCESIVAMENTE POR LOS 12 MESES*/

  async total_gain(req, res){
    var user = req.body.user_id;
    
    req.checkBody('user_id', 'El usuario no puede estar vacío').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).send(errors);
    }

    var userDB = await User.findById(user);
    console.log(userDB);
    if(!userDB)
      return res.json({
        status: 401,
        message: 'El usuario no existe'
      });
    else{
      //array
      var months_array = [];

      //for months
      var start = new Date(2019, 0, 1);
      var end = new Date(2019, 1, 1);

      //user's posts
      var postsDB = await Post.find({user: userDB._id, status: 1});
      var actionDB = await Action.findOne({name: 'Comprar'}); 

      if(!actionDB)
        return res.json({
          status: 401,
          message: 'No existe una accion con este nombre en la base de datos',
        });
      else{
        for(var i=0; i<12; i++){
          var monthly_total = 0;
          for(var j=0; j<postsDB.length; j++){
            //action posts 
            var actionPostsDB = await ActionPost.find({post: postsDB[j]._id, action: actionDB._id, status: 1});
            var actionPostsDB_array = [];
            for(var m=0; m<actionPostsDB.length; m++)
              actionPostsDB_array.push(actionPostsDB[m]._id);

            //get purchases 
            var purchasesDB = await Purchase.find({action_post: {$in: actionPostsDB_array}, status: {$in: [5,6]}, date: {"$gte": start, "$lt": end}});
            monthly_total += postsDB[j].price*purchasesDB.length;
          }
          var weekly_total = await this.weeks(i, postsDB, actionDB);
          var data = {
            month: i+1,
            monthly_total: monthly_total,
            weeks: weekly_total
          };
          months_array.push(data);
          start.setMonth(start.getMonth()+1);
          end.setMonth(end.getMonth()+1);
        }
        return res.json({
          status: 200,
          months: months_array
        })

      }
    }
  }

  async popular_users(req, res){
    var rolesDB = await Role.findOne({name: 'Usuario'});
    var usersDB = await User.find({status: 1, role: rolesDB._id});
    var actionDB = await Action.findOne({name: 'Comprar'}); 

    if(!rolesDB)
      return res.json({
        status: 401,
        message: 'No existe un rol con este nombre'
      })

    if(!actionDB)
      return res.json({
        status: 401,
        message: 'No existe una accion con este nombre'
      })
    else{
      console.log(usersDB);
      var users_array = [];
      for(var i=0; i<usersDB.length; i++){
        var total = 0;
        var postsDB = await Post.find({user: usersDB[i]._id});
        var userProfileDB = await UserProfile.findOne({user: usersDB[i]._id});

        for(var j=0; j<postsDB.length; j++){
          //action posts 
          var actionPostsDB = await ActionPost.find({post: postsDB[j]._id, action: actionDB._id, status: 1});
          var actionPostsDB_array = [];
          for(var m=0; m<actionPostsDB.length; m++)
            actionPostsDB_array.push(actionPostsDB[m]._id);

          //get purchases 
          var purchasesDB = await Purchase.find({action_post: {$in: actionPostsDB_array}, status: {$in: [5,6]}});
          total += postsDB[j].price*purchasesDB.length;
        }

        var data = {
          id: usersDB[i]._id,
          email: usersDB[i].email,
          name: userProfileDB.name,
          surname: userProfileDB.surname,
          image: userProfileDB.image,
          total_gain: total
        }
        console.log(data);
        users_array.push(data);
      }

      return res.json({
        status: 200,
        popular_users: users_array.sort((a, b) => parseFloat(b.total_gain) - parseFloat(a.total_gain))
      })
    }

  }

  async averageUserScore(req, res){ 
    const user = req.body.user;

    req.checkBody('user', 'El id del usuario no puede estar vacia').notEmpty();
      let errors = req.validationErrors();

      if(errors){
        return res.status(400).json({
          status: 400,
          errors 
        });
      }

    //check if the user exists
    User.findById(user, async function(error, userDB){
      if(error)
        return res.status(500).json({
          status: 500,
          error
        });
      if(!userDB)
        return res.json({
          status: 401,
          message: 'El usuario no existe'
        });
      else{

        //get posts from user
        var postsDB = await Post.find({user: user});
        var postsArray = [];
        for(var i = 0; i < postsDB.length; i++){
          postsArray.push(postsDB[i]._id);
        }

        let action = await Action.find({name: 'Comprar'});
        
        //get actionposts associated with post
        var actionPostsDB = await ActionPost.find({post: {$in: postsArray}, action: action[0]._id});
        var actionPostsArray = [];

        for(var i = 0; i < actionPostsDB.length; i++){
          actionPostsArray.push(actionPostsDB[i]._id);
        }


        //get purchases from actionposts
        Purchase.find({action_post: {$in: actionPostsArray}}, async function(error, purchasesDB){
          if(error)
            return res.status(500).json({
              status: 500,
              error 
            });
          if(!purchasesDB)
            return res.json({
              status: 401,
              message: 'No se han encontrado solucitudes de compra',
            });
          else{
            console.log(purchasesDB);

            let average = 0;

            if(purchasesDB.length <= 0)
              return res.json({
                status: 200,
                message: 'Promedio de puntuacion del vendedor',
                average: average
              })

            let acum_score = 0

            for(var i=0; i<purchasesDB.length; i++){
              acum_score += purchasesDB[i].score
            }

            average = acum_score / purchasesDB.length

            return res.json({
              status: 200,
              message: 'Promedio de puntuacion del vendedor',
              average: average
            })            

          }
        });
      }
    })
  }

}

module.exports = new UserController();
