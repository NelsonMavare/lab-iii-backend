"use-strict"
const User = require('../models/User');
const UserProfile = require('../models/UserProfile');
const Category = require('../models/Category');
const UserCategory = require('../models/UserCategory');
const Post = require('../models/Post');
const PostTransformer = require('../transformers/PostTransformer');
const UserCategoryTransformer = require('../transformers/UserCategoryTransformer');
const CategoryTransformer = require('../transformers/CategoryTransformer')

class UserCategoryController{

	constructor(){
		this.usersByCategory = this.usersByCategory.bind(this);
		this.follow = this.follow.bind(this);
		this.unfollow = this.unfollow.bind(this);
		this.followed_categories = this.followed_categories.bind(this);
		this.timeline = this.timeline.bind(this);
		this.followed_categories = this.followed_categories.bind(this);
	}

	findUser(user_profiles, user){
    var result = false
    user_profiles.map(user_profile => {
      if(user._id.toString() == user_profile.user.toString()){
        result = user_profile  
      }
    })

    return result
  }

  async usersByCategory(categoriesDB){

    let user_ids = []

    categoriesDB.forEach((category) => {
      user_ids.push(category.user._id)
    })

    console.log("USERIDS", user_ids)

    var user_profiles = await UserProfile.find({user: {$in: user_ids} });


    categoriesDB = categoriesDB.map((category) => {
      if(this.findUser(user_profiles, category.user) != false){
        var categorysend = Object.assign({}, category._doc, {
          user: Object.assign({}, category._doc.user._doc, {
            user_profile: this.findUser(user_profiles, category._doc.user._doc)
          })
        })
        return categorysend
      }
      return category
    }) 

    return categoriesDB

  }

	async follow(req, res){
		var user = req.body.user_id;
		var category = req.body.category_id;
		const usersByCategory = this.usersByCategory;

		req.checkBody('user_id', 'User es necesario.').notEmpty();
    	req.checkBody('category_id', 'La categoría es necesaria.').notEmpty();

		let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).send(errors);
	    }

		User.findById(user, function(error, userDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			else{
				if(!userDB)
					return res.json({
						status: 401,
						message: 'El usuario no existe'
					})
				else{
					UserCategory.find({user: userDB._id, category: category, status: 1}, async function(error, userCategoryDB){
						if(error)
							return res.status(400).json({
								status: 400,
								error
							});
						if(!isEmptyObject(userCategoryDB))
							return res.json({
								status: 401,
								message: 'El usuario ya sigue esta categoría'
							});
						else{
							var newUserCategory = new UserCategory({
								user: user,
								category: category,
								follow_date: Date.now()
							});

							let userCategorySaved = await newUserCategory.save();

							UserCategory.findById(userCategorySaved._id).populate('category').populate({path:'user', populate: {path: 'role'}}).exec( async function(err, userCategoryDB) {
								if(error)
									return res.status(500).json({
										status: 500,
										error
									});
								else{
									
									var userCategory = [];
									userCategory.push(userCategoryDB);
									
									let categoriesDBWithUsers = await usersByCategory(userCategory)

									return res.json({
										status: 200,
										message: 'Se ha seguido la categoría cón éxito',
										userCategory: UserCategoryTransformer.transform(categoriesDBWithUsers[0])
									})
								}
							})
						}
					});
				}
			}
		});
	}

	async unfollow(req, res){
		var user = req.body.user_id;
		var category = req.body.category_id;
		const usersByCategory = this.usersByCategory;

		User.findById(user, function(error, userDB){
			if(error)
				return res.status(500).json({
					status:500,
					error
				});
			if(!userDB)
				return res.json({
					status: 401,
					message: 'El usuario no existe'
				});
			else{
				UserCategory.findOne({user: userDB._id, category: category, status: 1}, async function(error, userCategoryDB){
					if(error)
						return res.status(500).json({
							status: 500, 
							error
						});
					if(!userCategoryDB)
						return res.json({
							status: 400,
							message: 'El usuario no se encuentra siguiendo esa categoría'
						});
					else{
						console.log(userCategoryDB);
						userCategoryDB.status = 2;
						let userCategorySaved = await userCategoryDB.save();

							UserCategory.findById(userCategorySaved._id).populate('category').populate({path:'user', populate: {path: 'role'}}).exec( async function(err, userCategoryDB) {
								if(error)
									return res.status(500).json({
										status: 500,
										error
									});
								else{
									
									var userCategory = [];
									userCategory.push(userCategoryDB);
									
									let categoriesDBWithUsers = await usersByCategory(userCategory)

									return res.json({
										status: 200,
										message: 'Se ha dejado de seguir esta categoría',
										userCategory: UserCategoryTransformer.transform(categoriesDBWithUsers[0])
									})
								}
							})
						}
				});
			}
		});
	}

	async timeline(req, res){
		var user = req.body.user_id;
		const usersByCategory = this.usersByCategory;

		User.findById(user, function(error, userDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			if(!userDB)
				return res.json({
					status: 401,
					message: 'El usuario no existe'
				});
			else{
				UserCategory.find({user: userDB._id, status: 1}, function(error, userCategoriesDB){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						});
					if(isEmptyObject(userCategoriesDB))
						return res.json({
							status: 401,
							message: 'El usuario no sigue ninguna categoría'
						});
					else{
						var userCategoriesArray = [];
						for(var i = 0; i < userCategoriesDB.length; i++){
						   userCategoriesArray.push(userCategoriesDB[i].category);
						}

						Post.find({category: {$in: userCategoriesArray}}).populate('user').populate('category').exec(async function(error, postsBD){
							if(error)
								return res.status(400).json({
									status: 400,
									error
								});
							if(!postsBD)
								return res.json({
									status: 401,
									message: 'No existen posts en ninguna de esas categorías'
								})
							else{
								let postsBDUser = await usersByCategory(postsBD)
								return res.json({
									status: 200,
									message: 'Posts de las categorías seguidas por el usuario',
									posts: PostTransformer.transform(postsBDUser)
								})
							}
						});	
					}
				});
			}
		});
	}

	//ESTRUCTURA DEL JSON QUE SE RETORNA
	/*{
    "status": 200,
    "categories": [
        {
            "id": "5db91a92e21df420285ca5cb",
            "name": "Categoria 1",
            "description": "Esta es una categoria",
            "status": 1,
            "follower_count": 1
        }
    ]*/

	async total_followers(req, res){
		
		var categoriesDB = await Category.find({status: 1});
		var categories_array = [];

		for(var i=0; i<categoriesDB.length; i++){
			var userCategoriesDB = await UserCategory.find({category: categoriesDB[i]._id, status: 1});
			var data = {
				id: categoriesDB[i]._id,
				name: categoriesDB[i].name,
				description: categoriesDB[i].description,
				status: categoriesDB[i].status,
				follower_count: userCategoriesDB.length
			};
			categories_array.push(data);
		};
		console.log(categories_array);
		return res.send({
			status: 200,
			categories: categories_array
		});

	}

	async followed_categories(req, res){
		var user = req.body.user_id;

		req.checkBody('user_id', 'El usuario es necesario.').notEmpty();

		let errors = req.validationErrors();

	    if(errors){
	      return res.status(400).send(errors);
	    }

	    User.findById(user, async function(error, userDB){
	    	if(error)
	    		return res.status(500).json({
	    			status: 500,
	    			error
	    		})
	    	if(!userDB)
	    		return res.json({
	    			status: 401,
	    			message: 'El usuario no existe'
	    		})
	    	else{
	    		var categories_array = [];
	    		var userCategoriesDB = await UserCategory.find({user: userDB._id, status: 1});
				for(var i = 0; i < userCategoriesDB.length; i++){
				   categories_array.push(userCategoriesDB[i].category);
				}
				console.log(categories_array);

				Category.find({_id: {$in: categories_array}}, function(error, categoriesDB){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						})
					if(categoriesDB.length == 0)
						return res.json({
							status: 401,
							message: 'El usuario no sigue ninguna categoria'
						})
					else
						return res.json({
							status: 200,
							message: 'Categorias que el usuario sigue',
							categories: CategoryTransformer.transform(categoriesDB)
						})
				});

	    	}
	    });

	}

	async popular_categories(req, res){
		var categoriesDB = await Category.find({status: 1});
		var categories_array = [];

		for(var i=0; i<categoriesDB.length; i++){
			var userCategoriesDB = await UserCategory.find({category: categoriesDB[i]._id, status: 1});
			var data = {
				id: categoriesDB[i]._id,
				name: categoriesDB[i].name,
				description: categoriesDB[i].description,
				status: categoriesDB[i].status,
				follower_count: userCategoriesDB.length
			};
			categories_array.push(data);
		};
		categories_array.sort((a, b) => parseFloat(b.follower_count) - parseFloat(a.follower_count));
		return res.send({
			status: 200,
			categories: categories_array
		});
	}


}

function isEmptyObject(obj) {
  return !Object.keys(obj).length;
}

module.exports = new UserCategoryController();