"use strict";
const Privilege = require('../models/Privilege');
const PrivilegeTransformer = require("../transformers/PrivilegeTransformer");

class PrivilegeController{
	constructor(){
		this.create = this.create.bind(this);
		this.update = this.update.bind(this);
		this.show = this.show.bind(this);
		this.index = this.index.bind(this);
		this.delete = this.delete.bind(this);

	}

	async create(req, res){
		const name = req.body.name;
		const description = req.body.description;

		req.checkBody('name', 'El campo nombre no puede estar vacio').notEmpty();
		let errors = req.validationErrors();

		if(errors){
			return res.status(400).send(errors);
		}

		else{
			var current_date = Date.now();
			let privilege = new Privilege({
				name: name,
				description: description,
				created_at: current_date,
				updated_at: current_date
			});

			privilege.save(function(error, privilegeDB){
				if(error){
					return res.status(500).json({
						status: 500,
						error
					})
				}
				else{
					return res.json({
						status: 200,
						message: 'Privilegio registrado exitosamente',
						privilege: PrivilegeTransformer.transform(privilegeDB)
					});	
				}
			});
		}
	}

	async update(req, res){
		const name = req.body.name;
		const description = req.body.description;

		req.checkBody('name', 'El campo nombre no puede estar vacio').notEmpty();
		let errors = req.validationErrors();

		if(errors){
			return res.status(500).json({
				status: 500,
				errors
			});
		}
		else{
			var id = req.params.id;
			Privilege.findOne({_id: id}, function(error, privilege){
				var current_date = Date.now();
				privilege.name = name;
				privilege.description = description;
				privilege.updated_at = current_date;

				privilege.save(function(error, privilegeDB){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						});
					else{
						return res.json({
							status: 200,
							message: 'Cambios realizados exitosamente',
							privilege: PrivilegeTransformer.transform(privilegeDB)
						});
					}
				});
			});
		}
	}

	async show(req, res){
		var id = req.params.id;
		Privilege.findOne({_id: id}, function(error, privilegeDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			else{
				return res.json({
					status: 200,
					message: 'Privilegio cargado con éxito',
					privilege: PrivilegeTransformer.transform(privilegeDB)
				});
			}
		});
	}

	async index(req, res){
		Privilege.find(function(error, privilegesDB){
			if(error)
				return res.status(500).json({
					status: 500,
					error
				});
			else{
				return res.json({
					status: 200,
					privileges: PrivilegeTransformer.transform(privilegesDB)
				});
			}

		});
	}

	async delete(req, res){
		var id = req.params.id;
		Privilege.findOne({_id: id}, function(error, privilege){
			if(error)
				return res.status(400).send(error);
			else{
				var current_date = Date.now();
				privilege.updated_at = current_date;
				privilege.status = 2;
				privilege.save(function(error, privilegeDB){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						});
					else
						return res.json({
							status: 200,
							message: 'Se ha eliminado el registro',
							privilege: PrivilegeTransformer.transform(privilegeDB)
						});
				});
			}
		});
	}

}

module.exports = new PrivilegeController();
