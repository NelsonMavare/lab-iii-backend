"use strict";
const Comment = require('../models/Comment');
const Purchase = require('../models/Purchase');
const CommentTransformer = require('../transformers/CommentTransformer');
const User = require('../models/User');
const UserProfile = require('../models/UserProfile');
const Action = require('../models/Action');
const ActionPost = require('../models/ActionPost');
const Post = require('../models/Post');


function findBuyer(user_profiles, user){
    var result = false
    user_profiles.map(user_profile => {
    	console.log(user_profile)
      if(user._id.toString() == user_profile.user.toString()){
        result = user_profile  
      }
    })

    return result
}

async function buyersByPurchase(commentsDB){

    let user_ids = []		

    commentsDB.forEach((comment) => {
      user_ids.push(comment.purchase.buyer._id)
    })

    var user_profiles = await UserProfile.find({user: {$in: user_ids} });


    commentsDB = commentsDB.map((comment) => {
      if(findBuyer(user_profiles, comment.purchase.buyer) != false){
        var commentnew = Object.assign({}, comment._doc, {
          purchase: Object.assign({}, comment._doc.purchase._doc, {
	          buyer: Object.assign({}, comment._doc.purchase._doc.buyer._doc, {
	            buyer_profile: findBuyer(user_profiles, comment._doc.purchase._doc.buyer._doc)
	          })
          })
        })
        return commentnew
      }
      return comment
    }) 
    return commentsDB

}


class CommentController{

	constructor(){
	}

	//PUBLICAR UN COMENTARIO
	/*{
	"purchase": "5dccd849e19087069826f9c2",
	"message": "Mensaje del comentario }*/
	async publish_comment(req, res){
		var purchase = req.body.purchase;
		var message = req.body.message;
		var score = req.body.score;

		req.checkBody('purchase', 'La compra no puede estar vacia').notEmpty();
		req.checkBody('message', 'El mensaje del comentario no puede estar vacio').notEmpty();
		req.checkBody('score', 'La calificación no puede estar vacía').notEmpty();
		let errors = req.validationErrors();

	    if(errors){
	      return res.status(500).json({
	        status: 500,
	        errors
	      });
	    } 

	    Purchase.findByIdAndUpdate(purchase, {score}, async function(error, purchaseDB){
	    	if(error)
	    		return res.status(500).json({
	    			status: 500,
	    			error
	    		});
	    	if(!purchaseDB)
	    		return res.json({
	    			status: 401,
	    			message: 'La compra no existe'
	    		});
	    	else{
	    		if(purchaseDB.status == 5){
	    			var newComment = new Comment({
	    				message: message,
	    				purchase: purchaseDB._id 
	    			});

	    			console.log(newComment);
	    			purchaseDB.status = 6;
  					purchaseDB.save();
	    			var savedComment = await newComment.save();
	    			Comment.findById(savedComment._id)
	    			.populate({path: 'purchase', populate: {path: 'buyer'}})
	    			.exec(async function(error, commentDB){
	    				if(error)
	    					return res.status(500).json({
	    						status: 500,
	    						error
	    					});
	    				else{
	    					var comments = []
	    					comments.push(commentDB)
	    					let commentWithBuyer = await buyersByPurchase(comments)

	    					return res.json({
	    						status: 200,
	    						message: 'Comentario publicado exitosamente',
	    						comment: CommentTransformer.transform(commentWithBuyer)
	    					})

	    				}
	    			})
	    		}
	    		else{
	    			return res.json({
	    				status: 400,
	    				message: 'No se puede dejar un comentario en una compra no finalizada'
	    			})
	    		}
	    	}
	    });
	}

	//COMENTARIOS DE UN VENDEDOR
	async seller_comments(req, res){
		var user = req.body.user;

		req.checkBody('user', 'El usuario no puede estar vacio').notEmpty();
		let errors = req.validationErrors();

	    if(errors){
	      return res.status(500).json({
	        status: 500,
	        errors
	      });
	    } 

	    User.findById(user, async function(error, userDB){
	    	if(error)
	    		return res.status(500).json({
	    			status: 500,
	    			error
	    		});
	    	if(!userDB)
	    		return res.json({
	    			status: 401,
	    			message: 'El usuario no existe'
	    		});
	    	else{
	    		//get if of user's posts
	    		var postsDB = await Post.find({user: userDB._id});
	    		console.log(postsDB);
				var postsArray = [];
				for(var i = 0; i < postsDB.length; i++){
				   postsArray.push(postsDB[i]._id);
				}

				//get id of action buy
				var actionDB = await Action.findOne({name: 'Comprar'});

				//get actionposts 
				var actionPostsDB = await ActionPost.find({action: actionDB._id, post: {$in: postsArray}});
				//array of actionposts ids
				var actionPostsArray = [];
				for(var i = 0; i < actionPostsDB.length; i++){
				   actionPostsArray.push(actionPostsDB[i]._id);
				}

				//get purchases
				var purchasesDB = await Purchase.find({action_post: {$in: actionPostsArray}});
				var purchasesArray  = [];
				for(var i = 0; i < purchasesDB.length; i++){
				   purchasesArray.push(purchasesDB[i]._id);
				}

				//FINALLY get comments
				Comment.find({purchase: {$in: purchasesArray}})
				.populate({path: 'purchase', populate: {path: 'buyer'}})
				.exec(async function(error, commentsDB){
					if(error)
						return res.status(500).json({
							status: 500,
							error
						});
					if(!commentsDB)
						return res.json({
							status: 401,
							message: 'No se encontraron comentarios'
						});
					else{

						return res.json({
							status: 200,
							message: 'Comentarios del vendedor',
							comments: CommentTransformer.transform(await buyersByPurchase(commentsDB))
						})

					}
				})

	    	}
	    });
	}
}

module.exports = new CommentController();
