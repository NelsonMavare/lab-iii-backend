'use-strict'
const Category = require('../models/Category');
const CategoryTransformer = require('../transformers/CategoryTransformer')

class CategoryController {
	
  constructor() {
    this.index = this.index.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.show = this.show.bind(this);
    this.exists = this.exists.bind(this);
  }

  async exists(req, res, next){
    
    if(req.body.post){
      req.body = JSON.parse(req.body.post)
    }

    const id = req.body.category_id

    if(!id)
      return res.json({
        status: 400,
        message: 'Ingrese un id de la categoria'
      })

    Category.findById(id, (err, categoryDB) => {

      console.log('category', categoryDB)
      if(err)
        return res.status(500).json({
          status: 500,
          err
        })

      if(!categoryDB)
        return res.json({
          status: 401,
          message: 'La categoría no existe'
        })

      next();
    })    

  }

  async index(req, res, next){

  	Category.find({}, (err, categoriesDB) => {
  		if(err)
  			return res.json({
  				status: 500,
  				err
  			})

  		return res.json({
  			status: 200,
  			message: 'Categorías cargadas con éxito',
  			categories: CategoryTransformer.transform(categoriesDB)
  		})

  	})

  }

  async create(req, res, next){
  	const {name, description} = req.body

  	req.checkBody('name', 'El nombre es necesario').notEmpty();
    req.checkBody('description', 'La descripción es necesaria').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).json({
      	status: 400,
      	errors
      });
    }

    Category.findOne({name}, (e, categoryFoundDB) => {
    	if(e)
    		return res.status(500).json({
    			status: 500,
    			e
    		}) 

    	if(categoryFoundDB)
    		return res.json({
    			status: 500,
    			message: 'Ya existe una categoría con ese nombre'
    		})

    	let category = new Category({name, description})

    	category.save((err, categoryDB) => {
	    	
	    	if(err)
	    		return res.status(500).json({
	    			status: 500,
	    			err
	    		})

	    	return res.json({
	    		status: 200, 
	    		message: 'Categoría creada con éxito',
	    		category: CategoryTransformer.transform(categoryDB)
	    	})

    	})

    })

  }

  async show(req, res, next){
  	const {id} = req.params

  	if(!id)
  		return res.json({
  			status: 400,
  			message: 'Ingrese un id'
  		})

  	Category.findById(id, (err, categoryDB) => {

  		if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

  		if(!categoryDB)
  			return res.json({
  				status: 401,
  				message: 'La categoría no existe'
  			})

  		return res.json({
  			status: 200,
  			message: 'Categoría encontrada',
  			category: CategoryTransformer.transform(categoryDB)
  		})

  	})
  }

  async update(req, res, nex){
  	const {id} = req.params
  	const {name, description, status} = req.body

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

  	req.checkBody('name', 'El nombre es necesario').notEmpty();
    req.checkBody('description', 'La descripción es necesaria').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).json({
      	status: 400,
      	errors
      });
    }

    Category.findByIdAndUpdate(id, {name, description, status}, {new: true}, (err, categoryDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

    	if(!categoryDB)
    		return res.json({
    			status: 401,
    			message: 'La categoría no existe'
    		})

    	return res.json({
    		status: 200,
    		message: 'Datos de la categoría modificados exitosamente',
    		category: categoryDB
    	})

    })
  }

  async delete(req, res, next){
  	const {id} = req.params

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

    let oldCategory = await Category.findById(id);

    if(!oldCategory)
      return res.json({
        status: 401,
        message: 'La categoría no existe'
      })


    if(oldCategory.status === 2)
      return res.json({
        status: 401,
        message: 'La categoría ya se encuentra inactiva'
      })   

    Category.findByIdAndUpdate(id, {status: 2}, (err, categoryDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

    	return res.json({
    		status: 200,
    		message: 'Categoría eliminada exitosamente',
    		category: categoryDB
    	})

    })

  }

  async reactivate(req, res, next){
  	const {id} = req.params

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

    let oldCategory = await Category.findById(id);

    if(!oldCategory)
      return res.json({
        status: 401,
        message: 'La categoría no existe'
      })


    if(oldCategory.status === 1)
      return res.json({
        status: 401,
        message: 'La categoría ya se encuentra activa'
      })   

    Category.findByIdAndUpdate(id, {status: 1}, {new: true}, (err, categoryDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

    	return res.json({
    		status: 200,
    		message: 'Categoría reactivada exitosamente',
    		category: categoryDB
    	})

    })
  }

}

module.exports = new CategoryController();