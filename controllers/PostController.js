"use strict";
const Post = require('../models/Post');
const UserProfile = require('../models/UserProfile');
const PostTransformer = require("../transformers/PostTransformer");
const ActionPost = require('../models/ActionPost');
const Action = require('../models/Action');
const path = require('path');
const fs = require('fs');

class PostController {
  constructor() {
    this.index = this.index.bind(this);
    this.post = this.post.bind(this);
    this.usersByPost = this.usersByPost.bind(this);
    this.filter = this.filter.bind(this); 
    this.getPostsByUser = this.getPostsByUser.bind(this);
  }

  findUser(user_profiles, user){
    var result = false
    user_profiles.map(user_profile => {
      if(user._id.toString() == user_profile.user.toString()){
        result = user_profile  
      }
    })

    return result
  }

  async usersByPost(postsDB){

    let user_ids = []

    postsDB.forEach((post) => {
      user_ids.push(post.user._id)
    })

    console.log("USERIDS", user_ids)

    var user_profiles = await UserProfile.find({user: {$in: user_ids} });


    postsDB = postsDB.map((post) => {
      if(this.findUser(user_profiles, post.user) != false){
        var posust = Object.assign({}, post._doc, {
          user: Object.assign({}, post._doc.user._doc, {
            user_profile: this.findUser(user_profiles, post._doc.user._doc)
          })
        })
        return posust
      }
      return post
    }) 

    return postsDB

  }

  // Obtener posts de un usuario
  /*body: {
    user: "5dccdab1b9593b3760c9d3aa"
  }*/
  async getPostsByUser(req, res){
    const {user} = req.body

    req.checkBody('user', 'Usuario es requerido').notEmpty();

    let errors = req.validationErrors();

    //check for errors
    if(errors){
      return res.status(500).json({
        status: 500,
        errors
      });
    }

    Post.find({user: user}).populate('user').populate('category').exec( async (err, postsDB) => {

      if(err)
        return res.status(500).json({
          status: 500,
          err
        })

      if(postsDB.length === 0){
        return res.json({
          status: 401,
          message: "El usuario no posee publicaciones"
        })
      }

      let postsDBWithUsers = await this.usersByPost(postsDB)

      return res.json({
        status: 200,
        message: "Publicaciones cargadas con exito",
        posts: PostTransformer.transform(postsDBWithUsers)
      })

    })

  }

  async index(req, res){
    
    Post.find({}).populate('user').populate('category').exec( async (err, postsDB) => {
      if(err)
        return res.json({
          status: 500,
          err
        })

      
      let postsDBWithUsers = await this.usersByPost(postsDB)

      return res.json({
        status: 200,
        message: 'Posts cargados con éxito',
        posts: PostTransformer.transform(postsDBWithUsers)
      })

    })
  }

  async post(req, res){

    //Post data
    const title = req.body.title;
    const price = req.body.price;
    const user = req.body.user_id;
    const category = req.body.category_id;
    const description = req.body.description || '';
    const conditions = req.body.conditions;
    var images_length = 0

    if(req.files != undefined){
      images_length = Object.keys(req.files).length
    }

    console.log(images_length)

    //validate request
    req.checkBody('title', 'Titulo requerido').notEmpty();
    req.checkBody('price', 'Precio requerido').notEmpty();
    req.checkBody('user_id', 'El usuario es requerido').notEmpty();
    req.checkBody('category_id', 'La categoría es requerida').notEmpty();
    req.checkBody('conditions', 'Las condiciones son requeridas').notEmpty();

    let errors = req.validationErrors();

    //check for errors
    if(errors){
      return res.status(500).json({
        status: 500,
        errors
      });
    }

    if(!req.files || images_length === 0){
      return res.json({
        status: 500,
        message: 'Debes subir al menos un archivo'
      })
    }

    // prepare new unique name from images uploaded 
    let new_image_name = null

    var newPost = new Post({
      title,
      price,
      user,
      category,
      description,
      conditions
    });

    for(var i in req.files){
      var image = req.files[i];
      let allowed_extensions = ['jpg', 'jpeg', 'png'];
      let short_name = image.name.split('.');
      let extension = short_name[short_name.length -1];

      // Verify extension allowed
      if(allowed_extensions.indexOf(extension) < 0){
        return res.json({
          status: 500,
          message: 'Formato de archivo no válido, las extensiones permitidas son ' + allowed_extensions.join(', ')
        })
      }

      new_image_name =`${new Date().getMilliseconds()}.${extension}`;

      //upload
      image.mv('public/uploads/posts/'+new_image_name, (err) => {
        if (err){
          var pathImage = path.resolve(__dirname, `../public/uploads/posts/${new_image_name}`);
          if( fs.existsSync(pathImage) ){
            fs.unlinkSync(pathImage);
          }
          return res.status(500).json({
            status: 500,
            err
          });
        }
      });
      newPost.pictures.push(new_image_name);
    }

    newPost.save((err, postDB) => {
      
      if(err)
        return res.status(500).json({
          status: 500,
          err
        })


      Post.findById(postDB._id).populate('user').populate('category').exec( async (err, postFoundDB) => {

        if(err)
          return res.json({
            status: 500,
            err
          })

        let postArray = []
        postArray.push(postFoundDB)

        let postsDBWithUsers = await this.usersByPost(postArray);

        return res.json({
          status: 200,
          message: 'Se ha publicado tu producto con éxito',
          post: PostTransformer.transform(postsDBWithUsers[0])
        })
      })

    })
  }

  async filter(req, res){
    
    const category_id = req.body.category_id;

    req.checkBody('category_id', 'Categoría requerida').notEmpty();
    let errors = req.validationErrors();

    //check for errors
    if(errors){
      return res.status(500).json({
        status: 500,
        errors
      });
    }

     Post.find({category: category_id}).populate('user').populate('category').exec( async (err, postsDB) => {
      if(err)
        return res.json({
          status: 500,
          err
        })

      if(!Object.keys(postsDB).length)
        return res.json({
          status: 400,
          message: 'No se ha encontrado ningun post con esas características'
        });
       
      
      let postsDBWithUsers = await this.usersByPost(postsDB)

      return res.json({
        status: 200,
        message: 'Posts cargados con éxito',
        posts: PostTransformer.transform(postsDBWithUsers)
      })

    })
  }

  async likes_count(req, res){
    var post = req.body.post;

    Post.findById(post, async function(error, postDB){
      if(error)
        return res.status(500).json({
          status: 500,
          error
        });
      if(!postDB)
        return res.json({
          status: 401,
          message: 'El post no existe'
        })
      else{
        var actionDB = await Action.findOne({name: 'Me gusta'});
        ActionPost.find({post: post, action: actionDB._id, status: 1}, function(error, actionPostsDB){
          if(error)
            return res.status(500).json({
              status: 500,
              error
            })
          if(actionPostsDB.length == 0)
            return res.json({
              status: 200,
              likes_count: 0
            });
          else
            return res.json({
              status: 200,
              likes_count: actionPostsDB.length
            })
        });
      }
    });

  }

  async delete(req, res){
    var post = req.body.post_id;

    req.checkBody('post_id', 'Post requerida').notEmpty();
    let errors = req.validationErrors();

    //check for errors
    if(errors){
      return res.status(500).json({
        status: 500,
        errors
      });
    }

    Post.findOne({_id: post, status: 1}, async function(error, postDB){
      if(error)
        return res.status(500).json({
          status: 500,
          error
        })
      if(!postDB)
        return res.json({
          status: 401,
          message: 'El post no existe'
        });
      else{
        postDB.status = 2;
        console.log(postDB);
        var savedPostDB = await postDB.save();
        return res.json({
          status: 200,
          message: 'El post ha sido eliminado',
          post: savedPostDB
        })
      }
    });

  }

}

module.exports = new PostController();