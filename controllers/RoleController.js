'use-strict'
const Role = require('../models/Role');
const RoleTransformer = require('../transformers/RoleTransformer')

class RoleController {
	
  constructor() {
    this.index = this.index.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.show = this.show.bind(this);
  }

  async index(req, res, next){

  	Role.find({}, (err, rolesDB) => {
  		if(err)
  			return res.json({
  				status: 500,
  				err
  			})

  		return res.json({
  			status: 200,
  			message: 'Roles cargados con éxito',
  			roles: RoleTransformer.transform(rolesDB)
  		})

  	})

  }

  async create(req, res, next){
  	const {name, description} = req.body

  	req.checkBody('name', 'El nombre es necesario').notEmpty();
    req.checkBody('description', 'La descripción es necesaria').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).json({
      	status: 400,
      	errors
      });
    }

    Role.findOne({name}, (e, roleFoundDB) => {
    	if(e)
    		return res.status(500).json({
    			status: 500,
    			e
    		}) 

    	if(roleFoundDB)
    		return res.json({
    			status: 500,
    			message: 'Ya existe un rol con ese nombre'
    		})

    	let role = new Role({name, description})

    	role.save((err, roleDB) => {
	    	
	    	if(err)
	    		return res.status(500).json({
	    			status: 500,
	    			err
	    		})

	    	return res.json({
	    		status: 200, 
	    		message: 'Rol creado con éxito',
	    		role: RoleTransformer.transform(roleDB)
	    	})

    	})

    })

  }

  async show(req, res, next){
  	const {id} = req.params

  	if(!id)
  		return res.json({
  			status: 400,
  			message: 'Ingrese un id'
  		})

  	Role.findById(id, (err, roleDB) => {

  		if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

  		if(!roleDB)
  			return res.json({
  				status: 401,
  				message: 'El rol no existe'
  			})

  		return res.json({
  			status: 200,
  			message: 'Rol encontrado',
  			role: RoleTransformer.transform(roleDB)
  		})

  	})
  }

  async update(req, res, nex){
  	const {id} = req.params
  	const {name, description, status} = req.body

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

  	req.checkBody('name', 'El nombre es necesario').notEmpty();
    req.checkBody('description', 'La descripción es necesaria').notEmpty();
    req.checkBody('status', 'El status es necesario').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(400).json({
      	status: 400,
      	errors
      });
    }

    Role.findByIdAndUpdate(id, {name, description, status}, {new: true}, (err, roleDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		})

    	if(!roleDB)
    		return res.json({
    			status: 401,
    			message: 'El rol no existe'
    		})

    	return res.json({
    		status: 200,
    		message: 'Datos del rol modificado exitosamente',
    		role: RoleTransformer.transform(roleDB)
    	})

    })
  }

  async delete(req, res, next){
  	const {id} = req.params

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

    let oldRole = await Role.findById(id)

    if(!oldRole)
      return res.json({
        status: 401,
        message: 'El rol no existe'
      })

    if(oldRole.status === 2)
      return res.json({
        status: 401,
        message: 'El rol ya se encuentra inactivo'
      })   

    Role.findByIdAndUpdate(id, {status: 2}, {new: true}, (err, roleDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		}) 		

    	return res.json({
    		status: 200,
    		message: 'Rol eliminado exitosamente',
    		role: RoleTransformer.transform(roleDB)
    	})

    })

  }

  async reactivate(req, res, next){
  	const {id} = req.params

  	if(!id)
			return res.json({
				status: 400,
				message: 'Ingrese un id'
			})

    let oldRole = await Role.findById(id)

    if(!oldRole)
      return res.json({
        status: 401,
        message: 'El rol no existe'
      })

    if(oldRole.status === 1)
      return res.json({
        status: 500,
        message: 'El rol ya se encuentra activo'
      })   

    Role.findByIdAndUpdate(id, {status: 1}, {new: true}, (err, roleDB) => {

    	if(err)
    		return res.status(500).json({
    			status: 500,
    			err
    		}) 		

    	return res.json({
    		status: 200,
    		message: 'Rol reactivado exitosamente',
    		role: RoleTransformer.transform(roleDB)
    	})

    })
  }

}

module.exports = new RoleController();