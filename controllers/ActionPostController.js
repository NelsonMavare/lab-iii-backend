'use-strict'
const ActionPost = require('../models/ActionPost');
const PostTransformer = require('../transformers/PostTransformer');
const Post = require('../models/Post');
const User = require('../models/User');
const UserProfile = require('../models/UserProfile');
const mongoose = require('mongoose');
const ActionPostTransformer = require('../transformers/ActionPostTransformer');
//const ActionTransformer = require('../transformers/ActionTransformer')

class ActionPostController {

  constructor() {
    this.like = this.like.bind(this)
    this.findUser = this.findUser.bind(this);
    this.usersByPost = this.getUsersByAction.bind(this);
    this.remove_like = this.remove_like.bind(this);
    this.liked_posts = this.liked_posts.bind(this);
  }

  findUser(user_profiles, user){
    var result = false
    user_profiles.map(user_profile => {
      if(user._id.toString() == user_profile.user.toString()){
        result = user_profile  
      }
    })

    return result
  }

  async getUsersByAction(actionPostsDB){
    console.log(actionPostsDB)
    let user_ids = []

    actionPostsDB.forEach((action_post) => {
      user_ids.push(action_post.post.user._id)
    })

    console.log("USERIDS", user_ids)

    var user_profiles = await UserProfile.find({user: {$in: user_ids} });


    actionPostsDB = actionPostsDB.map((action_post) => {
      if(this.findUser(user_profiles, action_post._doc.post._doc.user) != false){
        var posust = Object.assign({}, action_post._doc, {
          post: Object.assign({}, action_post._doc.post._doc, {
            user: Object.assign({}, action_post._doc.post._doc.user._doc, {
              user_profile: this.findUser(user_profiles, action_post._doc.post._doc.user._doc)
            })
          })
        })
        return posust
      }
      return action_post 
    }) 

    return actionPostsDB

  }


  /*FORMATO JSON PARA LIKE Y REMOVE LIKE DE UN USUARIO
    {
    "post": "5dc489faad5766256463694e",
    "action": "5db91c0a7fbcd30e7caf8300",
    "user_id": "5dbd87586733fe44147bef6a"
    } 
	*/

  async like(req, res, next){
    const usersByPost = this.usersByPost;
    const post = req.body.post;
    const action = req.body.action;
    const user = req.body.user_id;

    req.checkBody('post', 'El post es necesario').notEmpty();
    req.checkBody('action', 'La acción es necesaria').notEmpty();
    let errors = req.validationErrors();

    if(errors)
      return res.status(500).json({
        status: 500,
        errors
      });
    

    //FIND USER FROM USER ID IN BODY
    User.findById(user, function(error, userDB){
      if(error){
        return res.status(500).json({
          status: 500,
          error
        });
      }

        //FIND USERPROFILE FROM USER ID
        UserProfile.findOne({user: userDB._id}, function(error, userProfileDB){
          if(error){
            return res.status(500).json({
              status: 500, 
              error
            });
          }


            //check that user didn't already like the post
            ActionPost.findOne({post: post, status: 1, _id: {$in: userProfileDB.liked_posts}}, function(error, actionPostFound){
              if(error){
                return res.status(500).json({
                  status:500,
                  error
                });
              }

              if(actionPostFound){
                return res.json({
                  status: 401,
                  message: 'El usuario ya le ha dado me gusta a este post'
                });
              }

            });

            //CREATE NEW ACTION POST
            var newActionPost = new ActionPost({
              action_date: Date.now(), 
              post: post,
              action: action
            });

            //SAVE NEW ACTION POST
            newActionPost.save(async function(error, actionPostDB){
              if(error)
                return res.status(500).json({
                  status: 500,
                  error
                });

                //ADD NEW ACTIONPOST TO THE COLLECTION OF ACTION POSTS IN USERPROFILE
                /*userProfileDB.liked_posts = []; /*<--- coloque esta linea aqui porque cuando se borran fisicame los action posts 
                de la bbdd no se borran los que estan guardados en este arreglo, entonces necesitaba limpiarlo*/
                userProfileDB.liked_posts.push(actionPostDB._id);
                var newUserProfileLikedPost = await userProfileDB.save();

                ActionPost.findById(actionPostDB).populate( 
                  {path: 'post', 
                  populate: {path: 'user'}
                }).populate(
                  {path: 'post', populate: {path: 'category'}
                }).populate('action')
                .exec( async function(error, userProfileActionPost){

                    if(error)
                      return res.status(400).json({
                        status: 400,
                        error
                      })


                    var actionPostToSend = []
                    actionPostToSend.push(userProfileActionPost)
                    
                    let postsDBWithUsers = await usersByPost(actionPostToSend);

                    return res.json({
                      status: 200,
                      message: 'Post guardado en me gusta',
                      post: ActionPostTransformer.transform(postsDBWithUsers[0])
                    });
                    
                });
            });
        });
    });
    
  }

  async remove_like(req, res){
    
    const usersByPost = this.usersByPost;
    const post = req.body.post; 
    const action = req.body.action;
    const user = req.body.user_id;


    req.checkBody('post', 'El post es necesario').notEmpty();
    req.checkBody('action', 'La acción es necesaria').notEmpty();
    let errors = req.validationErrors();

    if(errors){
      return res.status(500).json({
        status: 500,
        errors
      });
    }

    //FIND USER FROM USER ID IN BODY
    User.findById(user, async function(error, userDB){
      if(error)
        return res.status(500).json({
          status: 500,
          error
        });

      else{
        //FIND USERPROFILE FROM USER ID
        UserProfile.findOne({user: userDB._id}, async function(error, userProfileDB){
          if(error)
            return res.status(500).json({
              status: 500, 
              error
            });

          else{

            //check that user has liked the post
            ActionPost.findOne({post: post, status: 1, _id: {$in: userProfileDB.liked_posts}}, async function(error, actionPostFound){
              if(error)
                return res.status(500).json({
                  status:500,
                  error
                });
              if(!actionPostFound)
                return res.json({
                  status: 401,
                  message: 'El usuario todavía no le ha dado me gusta al post'
                });
              else{
                actionPostFound.status = 2;
                var actionPostSaved = await actionPostFound.save();

                ActionPost.findById(actionPostSaved._id).
                populate(
                  {path: 'post', 
                  populate: {path: 'user'}
                }).populate(
                  {path: 'post', 
                  populate: {path: 'category'}
                }).populate('action')
                .exec(async function(error, userActionPost){
                  if(error)
                    return res.status(500).json({
                      status: 500,
                      error
                    });
                  else{


                    var actionPostToSend = []
                    actionPostToSend.push(userActionPost)
                    
                    let postsDBWithUsers = await usersByPost(actionPostToSend);

                    return res.json({ 
                      status: 200,
                      message: 'Se ha removido el like del post',
                      actionPost: ActionPostTransformer.transform(postsDBWithUsers[0])
                    });
                  }
                });
              }
            });

          }

        });

      }

    });
  }

  //RETURNS LIST OF LIKED POSTS FROM AN SPECIFIC USER
  /*FORMATO JSON PARA TRAERSE LOS LIKES DEL USUARIO 
  {
    "user_id": "5dbd87586733fe44147bef6a"
  }*/

  async liked_posts(req, res){
    const usersByPost = this.usersByPost;
    const user = req.body.user_id;

    User.findById(user, function(error, userDB){
      if(error)
        return res.status(500).json({
          status: 500,
          error
        });
      else{
        console.log(userDB);
        UserProfile.findOne({user: userDB._id}, function(error, userProfileDB){
          if(error)
            return res.status(500).json({
              status: 500,
              error
            })
          else{
            console.log(userProfileDB);
            ActionPost.find({status: 1, _id: {$in: userProfileDB.liked_posts}}).populate(
                  {path: 'post', 
                  populate: {path: 'user'}
                }).populate(
                  {path: 'post', populate: {path: 'category'}
                }).populate('action')
                .exec( async function(error, userActionPostsDB){
                    if(error)
                      return res.status(400).json({
                        status: 400,
                        error
                      })
                    if(isEmptyObject(userActionPostsDB))
                      return res.json({
                        status: 401,
                        message: 'El usurio no le ha dado me gusta a ningun post'
                      })
                    else{
                      let postsDBWithUsers = await usersByPost(userActionPostsDB);
                      return res.json({
                        status: 200,
                        message: 'Posts a los que el usuario le ha dado me gusta',
                        posts: ActionPostTransformer.transform(postsDBWithUsers)
                      });
                    }
                });
          }
          //else end
        });
        //userprofile search end
      }
    });
    //user search end
  }

}

function isEmptyObject(obj) {
  return !Object.keys(obj).length;
}


module.exports = new ActionPostController();