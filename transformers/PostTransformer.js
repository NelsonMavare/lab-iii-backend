const UserTransformer = require('./UserTransformer');
const CategoryTransformer = require('./CategoryTransformer');

const format = (data) => {
    return {
      id: data._id,
      title: data.title,
      post_date: data.post_date,
      price: data.price,
      description: data.description,
      pictures: data.pictures,
      conditions: data.conditions,
      user: {
        id: data.user.id || data.user._id,
        email: data.user.email,
        user_profile:{
          id: data.user.user_profile._id,
          liked_post: data.user.user_profile.liked_posts,
          username: data.user.user_profile.username || '',
          name: data.user.user_profile.name || '',
          lastname: data.user.user_profile.surname || '',
          image: data.user.user_profile.image || '',
          status: data.user.user_profile.status || ''
        },
        status: data.user.status
      },
      category: CategoryTransformer.transform(data.category),
      status: data.status
    };
};

exports.transform = (data) => {
  console.log('DATA', data)
    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};