const CategoryTransformer = require('./CategoryTransformer');
const UserTransformer = require('./UserTransformer');
const format = (data) => {
    return {
      id: data._id || data.id,
      follow_date: data.follow_date,
      category: CategoryTransformer.transform(data.category),
      user: {
        user: data.user._id || data.user.id,
        email: data.user.email
      },
      status: data.status,
    };
};

exports.transform = (data) => {
  console.log('DATA', data)

    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};