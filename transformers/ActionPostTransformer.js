
const format = (data) => {
    return {
      id: data.id,
      post: {
        id: data.post.id || data.post._id,
        title: data.post.title,
        post_date: data.post.post_date,
        price: data.post.price,
        description: data.post.description,
        pictures: data.post.pictures,
        conditions: data.post.conditions,
        user: {
          id: data.post.user.id || data.post.user._id,
          email: data.post.user.email,
          user_profile:{
            id: data.post.user.user_profile._id,
            liked_post: data.post.user.user_profile.liked_posts,
            username: data.post.user.user_profile.username || '',
            name: data.post.user.user_profile.name || '',
            lastname: data.post.user.user_profile.surname || '',
            image: data.post.user.user_profile.image || '',
            status: data.post.user.user_profile.status || ''
          },
          status: data.post.user.status
        },
        category: {
          id: data.post.category.id,
          name: data.post.category.name
        },
        status: data.post.status
      },
      action: {
        action_id: data.action.id,
        name: data.action.name
      },
      status: data.status
    };
};

exports.transform = (data) => {
  console.log('DATA', data)

    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};