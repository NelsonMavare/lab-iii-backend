const CountryTransformer = require('./CountryTransformer')
const RoleTransformer = require('./RoleTransformer')

const format = (data) => {
    return {
        id: data.user.id,
        email: data.user.email,
        user_profile:{
          id: data._id,
          liked_post: data.liked_posts,
          username: data.username || '',
          name: data.name || '',
          lastname: data.surname || '',
          image: data.image || '',
          twitter: data.twitter || '',
          instagram: data.instagram || '',
          birthday: data.birthday || '',
          facebook: data.facebook || '',
          phone: data.telephone || '',
          sex: data.gender || '',
          country: CountryTransformer.transform(data.country),
          status: data.status || ''
        },
        role: RoleTransformer.transform(data.role) || '',
        status: data.user.status
    };
};

exports.transform = (data) => {
    console.log(data)
    return format(data);
};