const format = (data) => {
    return {
        id: data._id || data.id,
        name: data.name,
        status: data.status
    };
};

exports.transform = (data) => {
    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};