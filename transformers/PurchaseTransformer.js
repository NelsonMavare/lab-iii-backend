const PostTransformer = require('./PostTransformer')

const format = (data) => {
  console.log(data.action_post.post.user)
    return {
      id: data.id || data._id,
      requirements: data.description,
      status: data.status,
      cancel_description: data.cancel_description,
      date: data.date,
      pictures: data.pictures,
      buyer: {
        buyer_id: data.buyer.id || data.buyer._id,
        email: data.buyer.email,
        buyer_profile: {
          id: data.buyer.buyer_profile._id,
          liked_post: data.buyer.buyer_profile.liked_posts,
          username: data.buyer.buyer_profile.username || '',
          name: data.buyer.buyer_profile.name || '',
          lastname: data.buyer.buyer_profile.surname || '',
          image: data.buyer.buyer_profile.image || '',
          twitter: data.buyer.buyer_profile.twitter || '',
          instagram: data.buyer.buyer_profile.instagram || '',
          facebook: data.buyer.buyer_profile.facebook || '',
          phone: data.buyer.buyer_profile.phone || '',
          sex: data.buyer.buyer_profile.gender || '',
          country: data.buyer.buyer_profile.country,
          status: data.buyer.buyer_profile.status || ''
        }
      },
      action_post: {
        action_post_id: data.action_post.id,
        post: {
          id: data.action_post.post._id,
          title: data.action_post.post.title,
          post_date: data.action_post.post.post_date,
          price: data.action_post.post.price,
          description: data.action_post.post.description,
          pictures: data.action_post.post.pictures,
          conditions: data.action_post.post.conditions,
          category: {
            id: data.action_post.post.category.id || data.action_post.post.category._id,
            name: data.action_post.post.category.name,
            description: data.action_post.post.category.description,
            status: data.action_post.post.category.status
          },
          user: {
            id: data.action_post.post.user.id || data.action_post.post.user._id,
            email: data.action_post.post.user.email,
            user_profile:{
              id: data.action_post.post.user.user_profile._id,
              liked_post: data.action_post.post.user.user_profile.liked_posts,
              username: data.action_post.post.user.user_profile.username || '',
              name: data.action_post.post.user.user_profile.name || '',
              lastname: data.action_post.post.user.user_profile.surname || '',
              image: data.action_post.post.user.user_profile.image || '',
              status: data.action_post.post.user.user_profile.status || ''
            },
            status: data.action_post.post.user.status
          },
          status: data.action_post.post.status
        },
        action: data.action_post.action
      }
    };
};

exports.transform = (data) => {
  console.log('DATA', data)
    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};