const format = (data) => {
    return {
      id: data._id,
      message: data.message,
      publish_date: data.publish_date,
      purchase: {
        purchase_id: data.purchase._id,
        description: data.purchase.description,
        purchase_status: data.purchase.status,
        score: data.purchase.score,
        buyer: {
          id: data.purchase.buyer._id,
          email: data.purchase.buyer.email,
          buyer_profile: {
            id: data.purchase.buyer.buyer_profile._id,
            liked_post: data.purchase.buyer.buyer_profile.liked_posts,
            username: data.purchase.buyer.buyer_profile.username || '',
            name: data.purchase.buyer.buyer_profile.name || '',
            lastname: data.purchase.buyer.buyer_profile.surname || '',
            image: data.purchase.buyer.buyer_profile.image || '',
            status: data.purchase.buyer.buyer_profile.status || ''
          }
        }
      },
      status: data.status
    };
};

exports.transform = (data) => {
  console.log('DATA', data)

    if(Array.isArray(data)){
        return data.map(item => {
            return format(item);
        })
    }

    return format(data);
};