var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config');
var User = require('../models/User');

let authenticate = (req, res, next) => {
  if(!req.headers.authorization) {
    return res
      .status(403)
      .send({message: "Authorization header not found"});
  }

  var token = req.headers.authorization.split(" ")[1];

  User.findOne({token: token}, function(error, user){
    if(!user)
      return res.status(500).send({message: 'Token not associated to a user'});
    else{
      if(user.token_expiration_date <= Date.now())
        return res.status(401).send({message: 'The token has expired'});
    }
  });

  req.token = token;
  next();
}

// Roles handled on the app (with unique name)
/*
  name: 'Administrador'
  name: 'Usuario'
*/


let verifyAdminRole = (req, res, next) => {

    if(!req.headers.authorization) {
      return res
        .status(403)
        .send({message: "Authorization header not found"});
    }

    var token = req.headers.authorization.split(" ")[1];

    User.findOne({token: token}).populate('role').exec( function(error, user){
      if(!user)
        return res.status(500).send({message: 'Token not associated to a user'});
      else{
        if(user.role.name != 'Administrador')
          return res.status(401).send({message: 'User is not a admin.'});
      }
    });

    req.token = token;
    next();
};


module.exports = {
  authenticate,
  verifyAdminRole
}