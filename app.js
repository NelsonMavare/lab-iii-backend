//dependencies
require('dotenv').config();
const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const requests = require('./middlewares/requests');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const config = require('./config/database');
const RouterLoader = require('./routes');

const server_port = process.env.SERVER_PORT || 3000;
const web_port = process.env.WEB_URL || 'http://localhost:3001';

const fileUpload = require('express-fileupload');
const path = require('path');

//init application
let app = express();

app.use(fileUpload());
app.use(express.static(path.resolve(__dirname, './public')));

app.use(
  cors({
    origin: 'http://localhost:3001',//`${web_port}`,
    credentials: true
  })
);

//validator
// Express Validator Middleware
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

//body-parser
app.use(bodyParser.json());

//database configs
mongoose.connect(config.database);
let db = mongoose.connection;

//testing database
db.once('open', (err) => {
  if(err) throw new Error(err);
	console.log('DB CONNECTED');
});

db.on('error', (error) => {
	console.log(error);
})

app.use(requests);
app = RouterLoader.load(app)

app.listen(server_port, (err) => {
  if(err) throw new Error(err);
  console.log(`Server running on port ${ server_port }`);
});