const mongoose = require('mongoose');

//Role schema
let roleSchema = mongoose.Schema({
	name:{
		type: String,
		required: [true, 'El nombre es necesario']
	},
	description:{
		type: String,
		required: [true, 'La descripción es necesaria']
	},
	status:{
		type: Number,
		default: 1
	}
});

const Role = module.exports = mongoose.model('Role', roleSchema);


