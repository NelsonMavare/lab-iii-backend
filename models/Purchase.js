const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Action schema
let purchaseSchema = mongoose.Schema({
	description:{
		type: String,
		required: false
	},
	date: {
		type: Date,
		required: false,
		default: Date.now
	},
	status: {
		type: Number,
		required: true,
		default: 1
	},
	score:{
		type: Number, 
		required: false
	},
	action_post:{
		type: Schema.Types.ObjectId, 
		ref: 'ActionPost',
		required: true
	},
	cancel_description:{
		type: String,
		required: false
	},
	score:{
		type: Number,
		required: false
	},
	pictures: [],
	buyer:{
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	}
});

const Purchase = module.exports = mongoose.model('Purchase', purchaseSchema);


