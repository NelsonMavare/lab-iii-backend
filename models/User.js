const mongoose = require('mongoose');
const joi = require('@hapi/joi');
const Schema = mongoose.Schema;

//User schema
let userSchema = mongoose.Schema({
	email:{
		type: String,
		required: true
	},
	password:{
		type: String,
		required: true
	},
	status:{
		type: Number,
		default: 1
	},
	created_at:{
		type: Date,
		required: true
	},
	updated_at:{
		type: Date,
		required: true
	},
	last_login_at:{
		type: Date,
		required: false
	},
	token:{
		type: String,
		required: true
	},
	role:{
		type: Schema.Types.ObjectId,
		ref: 'Role'
	},
	token_expiration_date:{
		type: Date,
		required: true
	}

});

module.exports = mongoose.model('User', userSchema);


