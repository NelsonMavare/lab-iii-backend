const mongoose = require('mongoose');

//Category schema
let categorySchema = mongoose.Schema({
	name:{
		type: String,
		required: [true, 'El nombre es necesario']
	},
	description:{
		type: String,
		required: [true, 'La descripción es necesaria']
	},
	status:{
		type: Number,
		default: 1
	}
});

const Category = module.exports = mongoose.model('Category', categorySchema);


