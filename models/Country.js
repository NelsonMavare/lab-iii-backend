const mongoose = require('mongoose');
const joi = require('@hapi/joi');

//User schema
let countrySchema = mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	status:{
		type: Number,
		default: 1
	},
	created_at:{
		type: Date,
		required: true
	},
	updated_at:{
		type: Date,
		required: true
	}
});

module.exports = mongoose.model('Country', countrySchema);


