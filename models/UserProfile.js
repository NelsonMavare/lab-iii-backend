const mongoose = require('mongoose');
const joi = require('@hapi/joi');
const Schema = mongoose.Schema;


//User schema
let userProfileSchema = mongoose.Schema({
	country:{
		type: Schema.Types.ObjectId,
		ref: 'Country'
	},
	user:{
		type: Schema.Types.ObjectId, 
		ref: 'User'
	},
	liked_posts:[{
		type: Schema.Types.ObjectId,
		ref: 'ActionPost'
	}],
	name:{
		type: String,
		required: false
	},
	surname:{
		type: String,
		required: false
	},
	birthday:{
		type: Date,
		required: false
	},
	gender:{
		type: Number,
		required: false
	},
	telephone:{
		type: String,
		required: false
	},
	facebook: {
		type: String,
		required: false
	},
	instagram: {
		type: String,
		required: false
	},
	twitter: {
		type: String,
		required: false
	},
	status:{
		type: Number,
		default: 1
	},
	image: {
		type: String,
		required: false
	},
	created_at:{
		type: Date,
		required: true
	},
	updated_at:{
		type: Date,
		required: true
	}
});

module.exports = mongoose.model('UserProfile', userProfileSchema);
