const mongoose = require('mongoose');

//Action schema
let actionSchema = mongoose.Schema({
	name:{
		type: String,
		required: [true, 'El nombre es necesario'],
		unique: [true, 'El nombre debe ser único']
	},
	description:{
		type: String,
		required: [true, 'La descripción es necesaria']
	},
	status:{
		type: Number,
		default: 1
	}
});

const Action = module.exports = mongoose.model('Action', actionSchema);


