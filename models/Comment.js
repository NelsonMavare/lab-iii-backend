const mongoose = require('mongoose');

//Action schema
let commentSchema = mongoose.Schema({
	message:{
		type: String,
		required: [true, 'El mensaje es necesario']
	},
	publish_date:{
		type: Date,
		required: true,
		default: Date.now
	},
	status:{
		type: Number,
		required: true,
		default: 1
	},
	purchase:{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Purchase',
		required: [true, 'La compra relacionada es necesaria']
	},
});

const ActionPost = module.exports = mongoose.model('Comment', commentSchema);