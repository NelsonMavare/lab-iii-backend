const mongoose = require('mongoose');
const joi = require('@hapi/joi');

let privilegeSchema =  mongoose.Schema({
	name:{
		type: String,
		required: true
	},
	status:{
		type: Number,
		default: 1
	},
	description:{
		type: String,
		required: false
	},
	created_at:{
		type: Date,
		required: true
	},
	updated_at:{
		type: Date,
		required: true
	}
});

const Privilege = module.exports = mongoose.model('Privilege', privilegeSchema);