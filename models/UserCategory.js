const mongoose = require('mongoose');
const Schema = mongoose.Schema;


//Action schema
let userCategorySchema = mongoose.Schema({
	user:{
		type: Schema.Types.ObjectId, 
		ref: 'User'
	},
	category:{
		type: Schema.Types.ObjectId, 
		ref: 'Category'
	},
	follow_date:{
		type: Date,
		required: true
	},
	status: {
		type: Number,
		required: true,
		default: 1
	}
});

const UserCategory = module.exports = mongoose.model('UserCategory', userCategorySchema);


