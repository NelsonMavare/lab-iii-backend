const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Action schema
let postSchema = mongoose.Schema({

	title:{
		type: String,
		required: [true, 'El titulo es necesario']
	},
	post_date:{
		type: Date, 
		default: Date.now
	},
	price: {
		type: Number,
		required: [true, 'El precio es necesario']
	},
	description: {
		type: String,
		required: false
	},
	pictures: [],
	user:{
		type: Schema.Types.ObjectId, 
		ref: 'User'
	},
	conditions: {
		type: String,
		required: true
	},
	category:{
		type: Schema.Types.ObjectId, 
		ref: 'Category'
	},
	status:{
		type: Number,
		default: 1
	}
});

const Post = module.exports = mongoose.model('Post', postSchema);


