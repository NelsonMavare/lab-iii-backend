const mongoose = require('mongoose');

//Action schema
let actionPostSchema = mongoose.Schema({
	action_date:{
		type: Date,
		required: [true, 'La fecha de accion es necesaria']
	},
	action:{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Action',
		required: [true, 'La accion relacionada es necesaria']
	},
	post:{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Post',
		required: [true, 'El post relacionado es necesario']
	},
	status:{
		type: Number,
		required: true,
		default: 1
	}
});

const ActionPost = module.exports = mongoose.model('ActionPost', actionPostSchema);