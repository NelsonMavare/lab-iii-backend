// services.js
var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config');

exports.createToken = function(user) {
  var payload = {
    iat: moment().unix(),
    exp: moment().add(3, "months").unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};

